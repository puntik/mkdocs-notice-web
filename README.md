# Poznámkový blok


## Instalace mkdocs (ubuntu)

``` bash
apt-get update
apt install python3-pip

pip install mkdocs

pip install mkdocs-material
```

## development

```
mkdocs serve 
```

## deployment

Zatím jen z localhost (dev stroj).

~~~
mkdocs build

rsync -r site/ o:www/rem.cz/docs 
~~~

