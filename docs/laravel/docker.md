
# Laravel v docker

## Instalace

~~~ bash
docker run --rm -it \
--volume $PWD:/app \
composer create-project laravel/laravel src
~~~

Kody pro laravel jsou ted v adresari src

## Dockerfile pro vývoj

~~~ Dockerfile title="Dockerfile.dev"
FROM composer:latest AS composer

FROM php:8.1.16

COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# Install dependencies
RUN apt-get update && apt-get install -y zip unzip curl build-essential autoconf libcurl4-openssl-dev git # libicu-dev

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
# RUN docker-php-ext-install # pdo_mysql zip exif pcntl
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

WORKDIR /var/www

RUN echo "memory_limit = 128M" > /usr/local/etc/php/conf.d/memory-limit.ini
USER www

# kopirovani nepotrebuju, pripojuju jako volume
# COPY src/ /var/www

EXPOSE 8000

EXPOSE 9003

# spustit server
# CMD ["php", "-S", "0.0.0.0:8000", "-t", "./public"]

# nebo s vyuzitim artisan serve
CMD ["php", "artisan", "serve", "--host", "0.0.0.0"]
~~~

Spuštění:
~~~ bash
docker build -f Dockerfile.dev -t puntik/docker-only:0.1 .
docker run -it --rm -p 8000:8000 -v "$PWD/src:/var/www"  puntik/docker-only:0.1
~~~

nebo docker-compose:
~~~ yaml title="docker-compose.yaml"
services:
   app:
        build:
            context: .
            dockerfile: Dockerfile.dev
        image: puntik/docker-only:0.1
        container_name: docker-only
        restart: unless-stopped
        tty: true
        env_file:
            - ./src/.env
        environment:
            PHP_IDE_CONFIG: "serverName=app.loc"
            XDEBUG_SESSION: PHPSTORM
        working_dir: /var/www
        user: www
        volumes:
            - ./src:/var/www
        ports:
            - "8000:8000"
        networks:
            - app-network
networks:
    app-network:
        driver: bridge
~~~


