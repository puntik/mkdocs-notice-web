máme jednoduchý middleware, který kontroluje přítomnost hlavičky Country-Code v requestu vstupujícím do chráněného 
controlleru. Od middlewaru chceme, aby dál nepustil takový request, který hlavičku Country-Code buď neobsahuje nebo její 
hodnota je nevalidní. Současně hodnotu nalezené country vloží jako attribute k requestu.

Implementujeme tedy middleware.

~~~php
<?php

namespace App\Http\Middleware;

use App\Model\Repositories\CountryRepository;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckCountryCode
{

        /** @var CountryRepository */
        private $countryRepository;

        public function __construct(CountryRepository $countryRepository)
        {
                $this->countryRepository = $countryRepository;
        }

        /**
         * Handle an incoming request.
         *
         * @param  Request  $request
         * @param  \Closure $next
         *
         * @return mixed
         */
        public function handle(Request $request, Closure $next)
        {
                if (! $request->hasHeader('Country-Code')) {
                        return JsonResponse::create(
                        	['message' => 'Country-Code header missing.'], 
                        	400
                        );
                }

                try {
                        $country = $this->countryRepository->getByCode($request->header('Country-Code'));
                        $request->attributes->set('country', $country);
                } catch (ModelNotFoundException $e) {
                        return JsonResponse::create(
                        	['message' => 'Country-Code header is invalid.'], 
                        	400
                        );
                }

                return $next($request);
        }
}
~~~

Vznikají nám tři možné scénáře: 

 * všechno je v pořádku
 * příchozí request hlavičku vůbec neobsahuje
 * příchozí request hlavičku obsahuje, ale je nevalidní (např. není registrovaná)

Příklad možného řešení testů jednotlivých scénářů:

1 .. **Všechno je v pořádku**, middleware by tedy měl vracet výsledek funkce (Closure) určené druhým parametrem metody 
handle().
 
Funkci si můžeme namockovat tak, aby vracela nám známou hodnotu, například true a tu pak testovat. Současně bychom měli
otestovat i přítomnost atributu country v requestu.

~~~php
<?php

/**
 * @test
 */
public function it_processes_country_check_successfully()
{
	// Given
	$this->request->headers->set('Country-Code', 'cs');

	// When
	$handled = $this->checkCountryCodeMiddleware->handle(
		$this->request, 
		function (Request $request) {return true;}
	);

	// Then
	$this->assertTrue($handled);
	$this->assertTrue($this->request->attributes->has('country'));
}
~~~

2 .. **Vstupní request neobsahuje hlavičku Country-Code.** Middleware by měl být ukončen a výstupem response se statusem 400 
a zprávou o chybě. 

Otestujeme tedy, zda odpověď je skutečně JsonResponse se statusem 400.
 
~~~php
<?php

/**
 * @test
 */
public function it_fails_and_returns_400_when_country_code_missing()
{
	// When
	$handler = $this->checkCountryCodeMiddleware->handle(
		$this->request, 
		function (Request $request) { }
	);

	// Then
	$this->assertInstanceOf(JsonResponse::class, $handler);
	$this->assertEquals(400, $handler->status());
}
~~~

3 .. Obdobně zpracujeme i třetí scénář, kdy **request hlavičku obsahuje, ale je nevalidní** a selže při získání z 
repository. 

~~~php
<?php

/**
 * @test
 */
public function it_fails_and_returns_400_when_country_code_is_not_found()
{
	// Given
	$this->request->headers->set('Country-Code', 'incorrect_country_code');

	// When
	$handler = $this->checkCountryCodeMiddleware->handle(
		$this->request, 
		function (Request $request) { }
	);

	// Then
	$this->assertInstanceOf(JsonResponse::class, $handler);
	$this->assertEquals(400, $handler->status());
}
~~~

Obecné nastavení pro všechny testy můžeme dát do metody setUp, kde nastavíme parametry používané všemi testy.

~~~php
<?php

protected function setUp()
{
		parent::setUp();

		$countryRepository = $this->app->make(CountryRepository::class);

		$this->checkCountryCodeMiddleware = new CheckCountryCode($countryRepository);
		$this->request                    = new Request();
}
~~~

Zajímavý je řádek 7, kterým vytvoříme instanci repository pomocí containeru laravelu. Ostatní parametry testu jsou 
inicializovány pomocí new.

--- 

Co by ještě šlo vylepšit. V testech, ktere testují validitu hlavičky by mohl být assert na znění chyby v json odpovědi. 
Dále je možně vytvoření entity Country s testovaným kódem až před vlastním testem (teď to mám v seeders a plní se před 
testy).
