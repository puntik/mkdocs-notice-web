# Configuring XDebug in Docker with PhpStorm

## Introduction

XDebug is a powerful tool for debugging and profiling PHP applications, allowing developers to step through code, inspect variables, and profile performance. In this article, we'll walk through the steps to set up and configure XDebug in a Docker environment using PhpStorm as the IDE.

## Setting up XDebug in Dockerfile:

To enable XDebug in a Docker environment, you can include the following steps in your Dockerfile:

~~~Dockerfile title="Dockerfile"
RUN pecl install xdebug
RUN docker-php-ext-enable xdebug

RUN echo "xdebug.mode = coverage,debug,develop" > /usr/local/etc/php/conf.d/99-xdebug.ini
RUN echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/99-xdebug.ini
RUN echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/99-xdebug.ini
RUN echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/99-xdebug.ini
RUN echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/99-xdebug.ini
~~~
These lines install XDebug, configure its modes, and set up the connection parameters.

!!! Routing information

    For macOS, the client_host is designated as **host.docker.internal**. On Linux, it is identified as **172.17.0.1**.

To manage configuration parameters easily, add the following records to your .env file:

~~~dotenv title=".env"
XDEBUG_SESSION=PHPSTORM
PHP_IDE_CONFIG="serverName=localhost"
~~~

Include these variables in your docker-compose.yaml file using env_file:

~~~yaml title="docker-compose.yml"
services:
  app:
    env_file:
      - .env
~~~

### Configuring PhpStorm:

In PhpStorm, follow these steps:

Set up the server: Navigate to Settings -> PHP -> Servers. Ensure that the server name matches the one specified in your docker-compose.yaml (referenced from the .env file).

Map server files to the project: Configure file mappings so PhpStorm can understand the relationship between server files and your local project.

![Screenshot](../_images/xdebug_docker_php_01.png)

### In the Browser:

Install the [Xdebug helper](https://github.com/wrep/xdebug-helper-for-chrome) extension, usually a small green bug icon. This extension helps facilitate communication between the IDE and the browser.

## Remote Connection into Docker Container

### Establishing an SSH Tunnel

To connect to a remote Docker development container securely, you can create an SSH tunnel. This allows you to forward ports from the remote server to your local machine. In this example, we'll forward port 9003.

~~~bash
ssh -R 9003:localhost:9003 username@remote-server
~~~

This command establishes a reverse SSH tunnel, forwarding the remote server's port 9003 to your local machine.

### Configuring xdebug in Dockerfile
In your Dockerfile, ensure that xdebug is configured to connect to the correct host. While host.docker.internal works on macOS, on Linux, you might need to set xdebug.client_host to 172.17.0.1.

~~~Dockerfile title="Dockerfile"
RUN echo "xdebug.client_host=172.17.0.1" >> /usr/local/etc/php/conf.d/99-xdebug.ini
~~~
Adjust the xdebug.client_host according to your development environment.

### Configuring PhpStorm

In PhpStorm, configure the server to match the internal Docker container settings. This is crucial for proper integration with xdebug.

* Name: atari (Ensure it matches the already set PHP_IDE_CONFIG.)
* Host: 0.0.0.0
* Port: 8000

Note: PhpStorm may automatically prompt for changes to the server configuration after the initial connection, when the "Break at first line in PHP script" option is set.

## Conclusion

By following these steps, you can seamlessly integrate XDebug into your PHP development workflow within a Docker environment, ensuring efficient debugging and profiling. With PhpStorm as your IDE, you'll have a powerful set of tools at your disposal to enhance your PHP development experience. Happy coding!