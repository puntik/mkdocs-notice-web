# Installing and Configuring PHP on Gentoo Linux

Gentoo Linux offers a highly customizable and performance-oriented environment, making it a preferred choice for users who want fine-grained control over their system. This comprehensive guide walks you through the process of installing PHP on Gentoo, ensuring you have the latest version with all the necessary configurations.

This article provides step-by-step instructions to install PHP on Gentoo Linux. Starting from syncing the Portage tree to unmasking specific PHP versions, updating the world set, and configuring PHP to suit your requirements, the guide covers each aspect of the installation process. Additionally, it includes tips on managing USE flags, updating libraries, and selecting the preferred PHP version.

## Sync Portage Tree

~~~bash
sudo emerge --sync
~~~

## Unmask the package

Open the */etc/portage/package.accept_keywords* file and add a line to unmask the PHP 8.2/8.3 package.
~~~
=dev-lang/php-8.2* 
=dev-lang/php-8.3*
~~~

!!! Routing information
    
    Newer versions of Portage allow `package.accept_keywords` to be a directory, and unmasking information can be stored in separate files for each application.

    Be aware that currently, there is no memcached available in the Portage tree for PHP 8.3. 
    
## Update world set
Tim si nejsem jistej, co to udela, zatim preskakuju

~~~bash
sudo emerge -avuDN @world
~~~

    -a: Asks for confirmation before proceeding with the installation or upgrade.
    -v: Enables verbose output, providing more detailed information about the process.
    -u: Stands for "upgrade". It updates installed packages to the latest available versions.
    -D: Enables the --deep option, which considers the entire dependency tree of packages.
    -N: Enables the --newuse option, which checks for changes in USE flags and updates the packages accordingly.

    @world: This is a special set in Gentoo's Portage system that represents the entire set of installed packages on the system. 
    
Running emerge @world will update all the installed packages to their latest versions based on the Portage tree.

## Install php 8.2/8.3
~~~
sudo emerge -av dev-lang/php
~~~

!!! Compilation Time
 
    Note the approximate compilation time (35 minutes in my environment).


## Update make.conf:
Update the */etc/portage/make.conf* file to set **PHP_TARGETS**:
~~~
PHP_TARGETS=“php8-1 php8-2”
~~~

Recompile Libraries (if using memcached) and update PHP configuration:
~~~bash
emerge -avp  dev-php/pecl-memcached
~~~

## Select prefered version of php
Use eselect to select the preferred version for CLI and FPM:
~~~bash
eselect php list cli
eselect php set cli 3
~~~

~~~bash
eselect php list fpm
eselect php set fpm 3
~~~

## Useful links:

- [gentoo wiki](https://wiki.gentoo.org/wiki/PHP)
- [gentoo package](https://packages.gentoo.org/packages/dev-lang/php)
- [cheatseat](https://wiki.gentoo.org/wiki/Gentoo_Cheat_Sheet)
