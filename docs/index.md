## Inspirace pro obvykle řešené problémy

- [Vytvoření adresáře](https://bitbucket.org/snippets/puntik/5edK5n/how-to-create-a-new-directory)
- [Rychlé zjištění počtu řádků v souboru](https://bitbucket.org/snippets/puntik/xe9b9A/count-number-lines-in-file)
- [Nastavní možné velikosti nahrávaného souboru a délky prováděného skriptu](others/upload_size.md)
