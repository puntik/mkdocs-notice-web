### Editace kódu

- **⌘D** - duplikuje aktuání řádek 
- **⌘⌥T** - obalit vybraný text
- **⌘⇧⏎** - dokončit výraz
- **⌥↑**- rozšiřující se výběr textu
- **⌘⇧V** - chytré vkládání ze schránky
- **⌘F12** - otevře navigátor objektu

### Definice funkce

- **⌘B** - přejde na definici funkce
- **⌘⌥B** - nabídne implementace 
- **⌥Space** - zobrazí definici funkce

### Refaktorizace

- **⌘⌥C** - extrahuje výběr do konstanty
- **⌘⌥V** - extrahuje výběr do proměnné
- **⌘⌥M** - extrahuje výběr do metody 
- **⌘⌥N** - inline variable

### Navigace

- **⌘E** - naposledy otevřené soubory
- **⌘⇧⌫** - procházení zpět v historii úprav
- **⌘⇧[** - přepínání mezi taby editoru
- **⌘⇧A**- otevře dialog hledání akce php stormu
- **⌘↑**- otevře navigační lištu
- **⌥F12** - otevře terminál
- **⇧⇧** - dialog hledání všude

### Testování

- **↑⇧D** - run tests
- **⌥⌘R** - resume program
- **F8** - step over
- **F7** - step into
- **⌥⇧F7** - step into my code
- **⇧F8** - step out

