## Install virtual environment

~~~bash
python -m venv .venv
~~~

It creates a directory .venv with all files. It is useful to put it into .gitignore file.

~~~gitignore
.venv
~~~

## Activate an environment

It starts the environment for development.

~~~bash
source .venv/bin/activate
~~~

After work it is necessary to deactivate project
~~~bash
deactivate
~~~