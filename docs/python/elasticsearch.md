## Instalace

~~~
pip install elasticsearch
~~~

## Připojení: 

Konfigurace odpovídá nastavení v pydantic settings class z [následujícího článečku](./pydantic-settings.md).
~~~python
from elasticsearch import Elasticsearch

client = Elasticsearch(
    hosts=settings.elasticsearch.hosts,
    api_key=settings.elasticsearch.api_key.get_secret_value(),
    ca_certs=settings.elasticsearch.ca_certs,
    verify_certs=settings.elasticsearch.verify_certs,
    request_timeout=settings.elasticsearch.request_timeout,
    max_retries=settings.elasticsearch.max_retries,
)
~~~

## Bulk insert

~~~python
# items to be indexed
movies = []

# list of operations
actions = []

for counter, move in movies:
    # define elasticsearch operation to be done
	action = {'index': {'_index': 'movies'}}
	actions.append(action)

    # data
	actions.append(movie.json())

    # save chunks with 20 items 
	if counter % 20:
		client.bulk(operations=actions)

        # clear actions list
        actions.clear()
else:
    # save rest of records
	client.bulk(operations=actions)
~~~

## Bulk update

~~~python
    actions = []

    for counter, (file, es_id) in enumerate(get_hit_iterator()):
        # define operation to be done
        action = {'update': {'_index': 'movies', '_id': es_id}}
        actions.append(action)

        # what to update
        artist_list = parse_creators(file)
        people = [person.dict() for person in artist_list]

        # document data to be updated
        body = {
            "doc": {
                "actors": people
            }
        }

        action = body
        actions.append(action)

        # save 50 items chunk 
        if counter % 50 == 0:
            elasticsearch.bulk(operations=actions)

            # clear bulk update
            actions.clear()
    else:
        # save rest of data
        elasticsearch.bulk(actions)

~~~