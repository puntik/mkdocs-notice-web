Instalace
~~~bash
pip install pydantic
pip install pydantic-settings
~~~

Konfigurace
~~~env
ELASTICSEARCH_HOSTS=["http://localhost:9200"]
ELASTICSEARCH_API_KEY=''
ELASTICSEARCH_VERIFY_CERTS="True"
ELASTICSEARCH_CA_CERTS="./ca.crt"
ELASTICSEARCH_REQUEST_TIMEOUT=30
ELASTICSEARCH_MAX_RETRIES=5
~~~

Vlastni tridy pro nastaveni
~~~python
from os.path import dirname, join
from pydantic import SecretStr, HttpUrl
from pydantic_settings import BaseSettings, PydanticBaseSettingsSource, SettingsConfigDict


ENV_FILE = join(dirname(__file__), '../.env')


class ElasticsearchSettings(BaseSettings):
    hosts: List[str] = Field(default_factory=list)
    api_key: SecretStr
    verify_certs: Optional[bool] = True
    ca_certs: Optional[str] = './ca.crt'
    request_timeout: Optional[int] = 3
    max_retries: Optional[int] = 5

    model_config = SettingsConfigDict(
        env_file=ENV_FILE,
        env_prefix="ELASTICSEARCH_",
    )


class Settings(BaseSettings):
    elasticsearch: ElasticsearchSettings = ElasticsearchSettings()

    @classmethod
    def settings_customise_sources(
            cls,
            settings_cls: type[BaseSettings],
            init_settings: PydanticBaseSettingsSource,
            env_settings: PydanticBaseSettingsSource,  # useful on kubernetes and docker env file
            dotenv_settings: PydanticBaseSettingsSource,  # useful on local development
            file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        return init_settings, env_settings, dotenv_settings


settings = Settings()
~~~

Pouziti
~~~python
from settings import settings

api_key = settings.elasticsearch.api_key.get_secret_value()
~~~
