## Partial
It allows you to fix a certain number of arguments of a function and generate a new function.

~~~python
from functools import partial
  
# A normal function 
def func(a, b, c): 
    return a + b + c 
  
# A partial function that calls func with 
# a as 5, b as 6, and c as 7
partial_func = partial(func, 5, 6) 
  
# Calling partial_func()
print(partial_func(7))  # output: 18
~~~

As functions in python are objects, we can imagine internal implementation as following code:

~~~python
# simple definition of partial
class Partial:
    def __init__(self, func, *args):
        self.func = func
        self.args = args

    def __call__(self, *additional_args):
        return self.func(*(self.args + additional_args))

# A normal function from example above
def func(a, b, c): 
    return a + b + c

# Create partial function
partial_func = Partial(func, 5, 6)

# Call partial function
result = partial_func(7)

# Should print 18
print(result)
~~~

Of course, real implementation in functool is slightly complex.

[Link to functool documentation](https://docs.python.org/3/library/functools.html#functools.partial)

## Callable typehint

The Callable type hint is used to indicate that an object is "callable", like a function (as in the example). It doesn't tell anything about the function's arguments or return type unless specified.

~~~python
def multiply_setup(a: float) -> Callable:
    def multiply(b: float) -> float:
        return a * b

    return multiply

# Define variables as callable
double = multiply_setup(2)
triple = multiply_setup(3)

# Calling double() returns 6
print(double(3))

# Calling triple() returns 9
print(triple(3))
~~~

Summary: **Callable** is a type hint that indicates an object is callable (like a function), and **functools.partial** is a tool for producing new functions with some arguments already supplied.

## Nested functions

They are useful when we want to contain a function logic inside another function to encapsulate or hide it from the rest of the program, or when a certain operation is repeated within a function.

- **scope** - The inner function can access variables from the enclosing function (also known as **closure**). However, the same doesn't apply in reverse – the outer function can't access variables of the inner function.
- **encapsulation** - By using nested functions, we can hide inner function logic from the global scope. This can be particularly useful in larger programs to prevent naming clashes and to provide encapsulation.
- **higher order functions** - Functions can return functions. This is common in decorators, a common Python design pattern.

Example:
~~~python
def my_decorator(func):
    def wrapper():
        print("Before nested function.")
        func()
        print("After nested function.")

    return wrapper

def say_hello():
    print("Hello!")

# Decorate our function
say_hello = my_decorator(say_hello)
~~~
