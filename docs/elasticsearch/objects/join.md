Relace typu otázka odpověď nebo blog komentář.
Dalsi priklady: 

- faq - otázka odpověď
- recipe, ingredience
- job listnings and applications
- employee management - employee - manager
- product - brand
- team - player

Super fujka.

``` typescript
interface Team
{
  name: string;
  players: Player[];
}

interface Player
{
  name: string;
}

interface Contestant
{
  id: uuid;
  name: string;
  kind: ('team', 'player');
  parent: Contestant | null;
}
```

### Mapping a data
!!! danger "Omezení"
    - jen jeden join field v indexu 

```json
PUT contestants 
{
  "mappings": {
    "properties": {
      "id": {
        "type": "keyword"
      },
      "name": {
        "type": "text"
      },
      "kind": {
        "type": "keyword",
      },
      "parent": {
        "type": "join",
        "relations": {
          "team": "player"
        }
      }
    }
  }
}
```
Naplnit nejaka data:

!!! warning "Omezení"
    - musi byt u odpovedi s routing - data musi byt na stejnem shardu

```json
POST _bulk?routing=1
{"index": {"_index": "contestants", "_id": "1"}}
{"id": "1", "name": "Sparta", "kind": "team", "parent": {"name": "team"}}
{"index": {"_index": "contestants", "_id": "2"}}
{"id": "2", "name": "Slavia", "kind": "team", "parent": {"name": "team"}}
{"index": {"_index": "contestants", "_id": "3"}}
{"id": "3", "name": "Jakub Pešek", "kind": "player", "parent": {"name": "player", "parent": "1"}}
{"index": {"_index": "contestants", "_id": "4"}}
{"id": "4", "name": "Ladislav Krejčí", "kind": "player", "parent": {"name": "player", "parent": "1"}}
{"index": {"_index": "contestants", "_id": "5"}}
{"id": "5", "name": "Jan Kuchta", "kind": "player", "parent": {"name": "player", "parent": "1"}}
{"index": {"_index": "contestants", "_id": "6"}}
{"id": "6", "name": "Adam Karabec", "kind": "player", "parent": {"name": "player", "parent": "1"}}
{"index": {"_index": "contestants", "_id": "7"}}
{"id": "7", "name": "Tomáš Čvančara", "kind": "player", "parent": {"name": "player", "parent": "1"}}
{"index": {"_index": "contestants", "_id": "8"}}
{"id": "8", "name": "David Pavelka", "kind": "player", "parent": {"name": "player", "parent": "1"}}
{"index": {"_index": "contestants", "_id": "9"}}
{"id": "9", "name": "Mick van Buren", "kind": "player", "parent": {"name": "player", "parent": "2"}}
{"index": {"_index": "contestants", "_id": "10"}}
{"id": "10", "name": "Stanislav Tecl", "kind": "player", "parent": {"name": "player", "parent": "2"}}
{"index": {"_index": "contestants", "_id": "11"}}
{"id": "11", "name": "Lukáš Provod", "kind": "player", "parent": {"name": "player", "parent": "2"}}
{"index": {"_index": "contestants", "_id": "12"}}
{"id": "12", "name": "Matěj Jurásek", "kind": "player", "parent": {"name": "player", "parent": "2"}}
{"index": {"_index": "contestants", "_id": "13"}}
{"id": "13", "name": "David Douděra", "kind": "player", "parent": {"name": "player", "parent": "2"}}
{"index": {"_index": "contestants", "_id": "14"}}
{"id": "14", "name": "Jan Bořil", "kind": "player", "parent": {"name": "player", "parent": "2"}}
```
### Query:
- [Dokumentace](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-parent-id-query.html)

~~~ json
// Zajímají mě všechnu odpovědi na oázku s id 1
GET contestants/_search
{
  "query": {
    "parent_id": {
      "type": "player",
      "id": "1"
    }
  }
}

// Zajímají mě takové otázky, které mají alespoň jednu odpověď.
GET contestants/_search
{
  "query": {
    "has_child": {
      "type": "player",
      "query": {"match_all": {}},
      "min_children": 1
    }
  }
}

// Zajímají mě takové odpovědi, které jsou připojené k nějaké otázce.
GET contestants/_search
{
  "query": {
    "has_parent": {
      "parent_type": "team",
      "query": {
        "term": {
          "id": {
            "value": "1"
          }
        }
      }
    }
  }
}
~~~
Todo: spojit s agregací

- [Parent](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-parent-aggregation.html)
- [Childrem](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-children-aggregation.html)

## Settings to prevent mapping explosion
[See more](https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html)