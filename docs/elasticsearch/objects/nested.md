
Nested objekty jsou uloženy jako samostatné dokumenty.
Tím je možne se na ně dotazovat izolovaně.
V podstatě to simuluje relační vazbu známou ze světa relačních databází.

!!! danger "Pozor"

    Tím, že elasticsearch ukládá nested objekty jako samostatné dokumenty, je to docela žravý. 
    Je tedy potřeba přemýšlet, zda je nested model to pravé.

Opět použijeme příklad s knihami a autory.

``` typescript title="Typescript example"
interface Author {
    name: string;
    language: string;
    role: ( 'writter' | 'illustrator' | 'translator' )
}

interface Novel {
    title: string;
    author: Author[];
    tags: string[];
}
```

Mapping s nested objekty pro takovou strukturu pak může vypadat následovně:

``` json hl_lines="9" title="Mapping"
PUT novels
{
  "mappings": {
    "properties": {
      "title": {
        "type": "text"
      },
      "author": {
        "type": "nested",
        "properties": {
          "title": "text",
          "author": {
            "properties": {
                "name": "text",
                "role": "keyword"
            }
          }
        }
      }
    }
  }
}
```

Oproti minulému příkladu zavedeme typ nested u vnořeného dokumentu. Tím zajistíme izolované indexování.

~~~ json title="Naplnit data"
POST _bulk
{"index":{"_index":"novels"}}
{"title":"Tajuplný ostrov","author":[{"name":"Jules Verne","role":"writter", "language": "french"},{"name":"Věra Šťovíčková-Heroldová ","role":"translator"}, {"name": "Jules Férat", "role": "illustrator" }], "tags": ["dobrodružná", "cestování", "nautilus", "přátelství"]}
{"index":{"_index":"novels"}}
{"title":"Kupec benátský","author":[{"name":"William Shakespeare","role":"writter", "language": "english"},{"name":"Martin Hilský","role":"translator"}], "tags": ["klasika", "žid", "sheylock"]}
{"index":{"_index":"novels"}}
{"title":"Černí baroni","author":[{"name":"Miroslav Švandrlík","role":"writter", "language": "czech"}, {"name": "Neprakta", "role": "illustrator"}], "tags": ["Kefalín", "černí baroni", "totalita"]}
{"index":{"_index":"novels"}}
{"title":"Shakespearova Anglie: Portrét doby","author":[{"name":"Martin Hilský","role":"writter", "language": "czech"}], "tags": ["klasika", "rozbor"]}
{"index":{"_index":"novels"}}
{"title":"Zlatí hoši","author":[{"name":"Martin Nezval","role":"writter", "language": "czech"}], "tags": ["přátelství"]}
~~~

### Dotazy a agregace

Dotaz typu nested.

~~~ json hl_lines="4"
GET novels/_search
{
  "query": {
    "nested": {
      "path": "author",
      "query": {
        "bool": {
          "must": [
            {"match": {"author.name": "Martin Hilský"}},
            {"match": {"author.role.keyword": "writter"}}
          ]
        }
      }
    }
  }
}
~~~

Ve výsledku je již vidět, že elasticsearch vrátil správně data na otázku **Najdi knihy, jejihž autorem (writter) je Martin Hilský**.

~~~ json hl_lines="5 6 7"
GET novels/_search
{
  "aggs": {
    "by_nested_author": {
      "nested": {
        "path": "author"
      },
      "aggs": {
        "filtered_writters": {
          "aggs": {
            "writter_count": {
              "terms": {
                "field": "author.name.keyword",
              }
            }
          },
          "filter": {
            "term": {
              "author.role.keyword": {
                "value": "writter"
              }
            }
          }
        }
      }
    }
  }
}
~~~

Reverse nested agregace
~~~json
GET novels/_search
{
  "aggs": {
    "cosi": {
      "nested": {
        "path": "author"
      },
      "aggs": {
        "top": {
          "terms": {
            "field": "author.name.keyword",
            "size": 10
          },
          "aggs": {
            "xxx": {
              "reverse_nested": {
              },
              "aggs": {
                "ttt": {
                  "terms": {
                    "field": "tags",
                    "size": 10
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
~~~

TODO: dopsat:

  - sortování
  - zjistit, jak jsou data uložená, nějaka povídání o performance, kolik to sežere dat
  - podivat se na volbu [included_in_root](https://www.elastic.co/guide/en/elasticsearch/reference/current/nested.html) a included_in_parent.
