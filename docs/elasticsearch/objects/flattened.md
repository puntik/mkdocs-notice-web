Uloží data objektu jako json.
Je vytvořeno jediné pole v mappingu pro celý json object.
Fieldy, které nezná nejsou automaticky založeny (dynamic mapping).
Defaultní možná hlouka vnoření objektu je 20. Změna při nastavení mappingu parametrem depth_limit.
Zamezuje mapping exposion.
Omezená množina query a agregací, zhruba na úrovní keyword.

!!! danger "Hodnoty jsou interpretovány jako keyword"

    Nelze použít pro analyzované pole, neaplikují se žádné analyzátory, ani operace nad daty a čísly.
    Neměl by být použit pro indexování celého objektu.

Typické použití jsou různé seznamy properties, atributů a pod.
Při dotazu na jména fieldů nelze použít wildcard.

## Jak definovat mapping

``` typescript title=""
interface Author {
    name: string;
    language: string;
    role: ( 'writter' | 'illustrator' | 'translator' )
}

interface Novel {
    title: string;
    author: Author;
    attributes: Record<string, (string | boolean | number)>
}
```

~~~ json hl_lines="16"
PUT novels
{
    "mappings": {
        "properties": {
            "title": {
                "type": "text"
            },
            "author": {
                "properties": {
                    "name": {"type": "text"},
                    "language": {"type": "keyword"},
                    "role": {"type": "keyword"}
                }
            },
            "attributes": {
                "type": "flattened"
            }
        }
    }
}
~~~
### Jak se to uloží v indexu

~~~json
POST _bulk
{"index":{"_index":"novels"}}
{"title":"Dva tisíce mil pod mořem","author":[{"name":"Jules Verne","role":"writter"},{"name":"Věra Šťovíčková-Heroldová ","role":"translator"}], "attributes": {"genre": "sience fiction", "issuer": "SNDK", "cover_color": "blue", "pages": 123}}
{"index":{"_index":"novels"}}
{"title":"Romeo a Julie","author":[{"name":"William Shakespeare","role":"writter"},{"name":"Martin Hilský","role":"translator"}], "attributes": {"issuer": "NKL", "cover_color": "red", "pages": 234, "format": "a5" }}
{"index":{"_index":"novels"}}
{"title":"Shakespearova Anglie: Portrét doby","author":[{"name":"Martin Hilský","role":"writter"}], "attributes": {"genre": "literary criticism", "issuer": "NKL", "pages": 167, "audiobook": true }}
{"index":{"_index":"novels"}}
{"title":"Máj","author":[{"name":"Karel Hynek Mácha","role":"writter"}, {"name":"Rudolf Hrušínský","role":"reader"}], "attributes": {"genre": "poetry", "issuer": "NKL", "pages": 34, "audiobook": true }}

~~~

Uloží se to jako kompletní json do jediného fieldu.

~~~ json
{
    "title": "Dva tisíce mil pod mořem",
    "author.name": ["jules", "verne", "vera", "stovickova"],
    "attributes": "{issuer=SNDK, cover_color=blue, pages=23}"
}
~~~

Ziskani dat z flattened fieldu: 

~~~json hl_lines="3-5"
GET novels/_search
{
  "_source": [
    "attributes.issuer"
  ],
  "query": {
    "match_all": {}
  }
}

~~~


## Dotazy, agregace a řazení
Hledat se dá nad celým uloženým objektem, ale i pomocí tečkové notace nad jednotlivým klíčem.

Prohledá všechny nody fieldu labels.
Jednotlivé nody je možné prohledat pomocí tečkové notace.
Nad textovým polem to hledá vždy jako keyword - pole není nikdy analyzované.
Highlight není podporováno.

Podporovány jsou jen některé operace - většinou jako nad keyword. Žádné speciální handlování
pro numerické a datově položky není podporováno.

Možné query:

- exists
- term, terms a terms_set
- range
- prefix
- query_string a simple_query_string

!!! warning "Řažení jako string"

    Sortování se chová jako nad keyword, to znamená že se hodnota chápe jako string.

~~~ json title="Příklady dotazů"
GET novels/_search
{
  "query": {
    "exists": {
      "field": "attributes.audiobook"
    }
  }
}

GET novels/_search
{
    "query": {
        "term": {
            "attributes": "NKL"
        }
    }
}

GET novels/_search
{
    "query": {
        "term": {
            "attributes.issuer": "SNDK"
        }
    }
}

GET novels/_search
{
  "query": {
    "range": {
      "attributes.pages": {
        "gte": 200,
        "lte": 300
      }
    }
  }
}
~~~

``` json title="Příklady agregace"
GET novels/_search
{
  "query": {
    "match_all": {}
  },
  "aggs": {
    "by_issuer": {
      "terms": {
        "field": "attributes.issuer",
        "size": 10
      }
    }
  }
}
```

Co dál s některým omezením flattened pole? Podle issues (např. [43805/2019](https://github.com/elastic/elasticsearch/issues/43805), [61550/2020](https://github.com/elastic/elasticsearch/issues/61550)) existují otevřené tickety s požadavkem na numerické zpracování flattened pole (potřeba pro finanční analytiku).