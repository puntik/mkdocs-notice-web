Elastic search narozdíl od relačních databází pracuje s denormalizovanými daty.
To znamená, že data můžou být v indexu uložena duplicitně.
Rychlost vyhledávání je upřednostněno před normalizací a konzistencí uložených dat.
V praxi to znamená, že data, v relační databázi spojená joinem jsou v elasticsearch uložena přímo v daném objektu.

Nejjednodušší příklad je [objekt](https://www.elastic.co/guide/en/elasticsearch/reference/current/object.html), v našem připadě vazba kniha a autor.

~~~typescript
interface Author {
    name: string;
    language: string;
}

interface Novel {
    title: string;
    author: Author;
}
~~~

Tím, že elasticsearch pracuje s **json** daty, práce s objektem je jednoduchá a přímočará.

Mapping pro náš příklad:

~~~json
PUT novels
{
    "mappings": {
        "properties": {
            "title": {
              "type": "text"
            },
            "author": {
                "properties": {
                    "name": {
                      "type": "text"
                    },
                    "role": {
                      "type": "keyword"
                    }
                }
            }
        }
    }
}
~~~

Následně uložíme nějaká data.

~~~json
POST _bulk
{"index":{"_index":"novels"}}
{"title":"Dva tisíce mil pod mořem","author":{"name":"Jules Verne", "language": "french"}}
{"index":{"_index":"novels"}}
{"title":"Romeo a Julie","author":{"name":"William Shakespeare", "language": "english"}}
{"index":{"_index":"novels"}}
{"title":"Shakespearova Anglie: Portrét doby","author":{"name":"Martin Hilský", "language": "czech"}}
~~~

Reprezentace dat v indexu vypadá potom zhruba takhle:
~~~ json
{
    "title": ["Dva", "tisíce", "mil", "pod", "mořem"],
    "author.name": ["Jules", "Verne"],
    "author.language": ["french"]
}
~~~

### Sestavení dotazu a agregace

Z ukázky reprezentace dat vyplývá dotaz na jednotlivé prvky objektu ***author***. Například:

~~~json
GET novels/_search
{
    "query": {
        "term": {"author.language": "french"}
    }
}
~~~
Nebo trochu složitější:

~~~json
GET novels/_search
{
  "query": {
    "bool": {
      "must": [
        {"match": {"author.role": "writter"}},
        {"match": {"author.name": "Martin Hilský"}}
      ]
    }
  }
}
~~~

Agregace je podobně snadná, například zjištění počtu knih podle autorů:

~~~json
GET novels/_search
{
  "query": {
    "match_all": {}
  },
  "aggs": {
    "count_by_author": {
      "terms": {
        "field": "author.name.keyword"
      }
    }
  }
}
~~~

### Pole objektů

Co se ale stane, když uložíme **pole objektů**. Můžeme nechat indexovat napřiklad následující data:

~~~typescript
interface Author = {
    name: string;
    language: string;
    role: ( writter | illustrator | translator )
}

interface Novel = {
    title: string;
    author: Author[];
}
~~~

Naplníme nějakými daty:

~~~json
POST _bulk
{"index":{"_index":"novels"}}
{"title":"Tajuplný ostrov","author":[{"name":"Jules Verne","role":"writter", "language": "french"},{"name":"Věra Šťovíčková-Heroldová ","role":"translator"}, {"name": "Jules Férat", "role": "illustrator" }]}
{"index":{"_index":"novels"}}
{"title":"Kupec benátský","author":[{"name":"William Shakespeare","role":"writter", "language": "english"},{"name":"Martin Hilský","role":"translator"}]}
{"index":{"_index":"novels"}}
{"title":"Černí baroni","author":[{"name":"Miroslav Švandrlík","role":"writter", "language": "czech"}, {"name": "Neprakta", "role": "illustrator"}]}
~~~

V indexu jsou taková data reprezentována následovně:
~~~json
{
    title: "Tajuplný ostrov",
    author.name: ["Jules Verne", "Věra Šťovíčková-Heroldová", "Jules Férat"],
    author.role: ["writter", "translator", "illustrator"],
    author.language: ["french"]
}
~~~

Z téti ukázky uložení dat je zřejmé, že data jednotlivých objektů author jsou rozloženy do příslušných fieldů a neví o svých vazbách. 
Jinými slovy, není možné sestavit dotaz **Najdi knihy s autorem typu writter a jménem Martin Hilský**.
Tento zdánlivý nedostatek řeší typ [nested]().
