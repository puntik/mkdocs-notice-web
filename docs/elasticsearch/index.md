## Docker install ... 

[Dev pro tři nody a ssl připojení](https://bitbucket.org/puntik/elastic-kibana/src/v.8.15.0)

Nebo single node:


## Single node with kibana

``` Dockerfile title="Dockerfile"
FROM elasticsearch:7.17.28

RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install analysis-icu
```

``` yaml title="docker-compose.yml"
services:
    setup:
        image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
        command: >
            bash -c '
            echo "Waiting for Elasticsearch availability";
            until curl -s http://elasticsearch:9200 | grep -q "missing authentication credentials"; do sleep 5; done;
            echo "Setting kibana_system password";
            until curl -s -X POST -u "elastic:elastic" -H "Content-Type: application/json" http://elasticsearch:9200/_security/user/kibana_system/_password -d "{\"password\":\"${KIBANA_PASSWORD}\"}" | grep -q "^{}"; do sleep 5; done;
            echo "All done!";
            '
        networks:
            - elastic

    elasticsearch:
        depends_on:
            - setup
        # build:
        #   context: .
        image: docker.elastic.co/elasticsearch/elasticsearch:${STACK_VERSION}
        container_name: elasticsearch
        environment:
            - ELASTIC_PASSWORD=${ELASTIC_PASSWORD}
            - discovery.type=single-node
            - xpack.security.enabled=true
            - bootstrap.memory_lock=true
            - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
        mem_limit: ${MEM_LIMIT}
        ulimits:
            memlock:
                soft: -1
                hard: -1
        command: |
            bash -c '
                elasticsearch-plugin install analysis-icu;
                /bin/tini -- /usr/local/bin/docker-entrypoint.sh eswrapper'                
        volumes:
            - data01:/usr/share/elasticsearch/data
            - ./hunspell:/usr/share/elasticsearch/config/hunspell
        ports:
            - ${ES_PORT}:9200
            - 9300:9300
        networks:
            - elastic

    kibana:
        depends_on:
            - elasticsearch
        image: docker.elastic.co/kibana/kibana:${STACK_VERSION}
        container_name: kibana
        ports:
            - ${KIBANA_PORT}:5601
        environment:
            - ELASTICSEARCH_HOSTS=http://elasticsearch:9200
            - ELASTICSEARCH_USERNAME=kibana_system
            - ELASTICSEARCH_PASSWORD=${KIBANA_PASSWORD}
        mem_limit: ${MEM_LIMIT}            
        networks:
            - elastic

networks:
    elastic:
        driver: bridge

volumes:
    data01:
        driver: local
```

## What chatGPT says ..

Elasticsearch is a distributed, open-source search and analytics engine that can be used to store, search, and analyze large volumes of data in near-real-time.

Some of its key capabilities include:

- **Full-text search**: Elasticsearch provides advanced full-text search capabilities, including stemming, fuzzy matching, and phrase matching.
- **Text search**: Elasticsearch can search and analyze text data in real-time and provide highly relevant search results based on various search criteria and ranking algorithms.
- **Analytics**: Elasticsearch provides rich analytics capabilities, including aggregations, filters, and facets, that allow you to extract insights from your data and visualize them in various ways.
- **Real-time search and analysis**: Elasticsearch can process and analyze data in real-time, allowing you to monitor and respond to changes in your data as they happen.
- **Document-oriented**: Elasticsearch is document-oriented, meaning it stores data in a flexible, JSON-based format that allows you to easily map and index complex data structures.
- **Scalability**: Elasticsearch is designed to scale horizontally, meaning you can easily add more nodes to the cluster to increase storage capacity and query performance.

Overall, Elasticsearch is a powerful tool for storing, searching, and analyzing large volumes of data, and it's used in a wide range of applications, from enterprise search and e-commerce to log analysis and cybersecurity.

### Fulltext searching

Full-text searching in Elasticsearch refers to the ability to search for words or phrases within large volumes of unstructured text data, such as documents, web pages, or social media posts.

Elasticsearch uses an **inverted index** to store and retrieve text data efficiently. In this index, each term in the text is stored as a **separate document**, along with a list of the documents that contain that term. When a search query is executed, Elasticsearch searches through the index and returns the documents that match the query.

To perform a full-text search in Elasticsearch, you can use the **match query**, which searches for documents that contain the specified words or phrases in any field of the index. You can also use the **multi-match** query to search for a query string in multiple fields simultaneously.

Elasticsearch provides a number of features to enhance full-text search capabilities, including stemming (i.e., searching for variations of a word), fuzzy search (i.e., searching for similar terms), and synonym expansion (i.e., searching for different words with the same meaning).

Full-text searching in Elasticsearch is a powerful and efficient way to search through large volumes of unstructured text data, providing fast and accurate results.

### E-commerce search

Elasticsearch is also commonly used in e-commerce applications, where it can be used to power product search and recommendation systems. With Elasticsearch, you can build powerful search and recommendation algorithms that help users find the products they are looking for quickly and easily.

E-commerce search with Elasticsearch involves using the search engine to power the search functionality on an e-commerce website or application. Here are some of the ways Elasticsearch can be used for e-commerce search:

- **Product search**: Elasticsearch can be used to power product search on an e-commerce site, allowing users to search for products by keyword, category, brand, price range, and other attributes. With Elasticsearch, you can build a search engine that delivers fast and accurate results, even when dealing with large catalogs of products.
- **Autocomplete and suggestion**: Elasticsearch can be used to provide autocomplete and suggestion functionality as users type in their search queries. This helps users find the products they are looking for more quickly and easily, while also reducing the risk of misspellings or errors.
- **Faceted search:** Elasticsearch can be used to power faceted search, which allows users to filter search results based on attributes like color, size, material, and other product features. This makes it easier for users to find the exact products they are looking for, while also providing a more engaging and personalized shopping experience.
- **Recommendations**: Elasticsearch can be used to power recommendation systems that suggest products to users based on their search history, browsing behavior, and other factors. With Elasticsearch, you can build powerful recommendation algorithms that help users discover new products they might not have found otherwise.
- **Personalization**: Elasticsearch can be used to power personalized search and recommendations, tailoring the search results and product suggestions to the individual user's preferences and history. This helps create a more engaging and personalized shopping experience, which can increase customer satisfaction and loyalty.

Overall, Elasticsearch is a powerful tool for building fast, accurate, and personalized search and recommendation systems for e-commerce applications. By using Elasticsearch for e-commerce search, you can help users find the products they are looking for more quickly and easily, while also providing a more engaging and personalized shopping experience.

### Recommendation system

Implementing user profiling for a recommendation system with Elasticsearch involves collecting and analyzing data on user behavior, preferences, and other relevant information. Here's a general outline of how you can implement user profiling with Elasticsearch:

- **Collecting user data**: The first step is to collect data on user behavior, such as their search queries, product views, purchases, and other interactions with your site or application. You can use Elasticsearch to index and store this data, and to create mappings and fields that reflect the relevant user attributes.
- **Analyzing user data**: Once you have collected user data, you can use Elasticsearch's search and aggregation capabilities to analyze that data and extract insights about user behavior and preferences. For example, you can identify the most popular products or categories, the most common search queries, or the most frequent purchasing patterns.
- **Building user profiles**: Based on the insights you have gained from analyzing user data, you can build user profiles that reflect each user's preferences and behavior. These profiles can be stored in Elasticsearch as separate documents, and can be updated in real-time as users interact with your site or application.
- **Computing similarity scores**: To recommend products to users, you need to compute similarity scores between each user's profile and the products in your catalog. This involves using techniques like collaborative filtering or content-based filtering to identify products that are similar to the ones a user has shown an interest in.
- **Generating recommendations**: Based on the similarity scores, you can generate personalized recommendations for each user. These recommendations can be displayed on your site or application, and can be updated in real-time as users interact with your site or application.

Overall, implementing user profiling for a recommendation system with Elasticsearch requires a combination of data collection, analysis, and computation. With the right data and techniques, however, you can build a powerful recommendation system that helps users discover new products and increases engagement on your site or application.
