
## Trocha teorie

  - co je tokenizace - rozpad textu na tokeny - podle whitespace a pod ... 
  - co je lemmatizace - urceni lemmatu (zakladniho slovniho tvaru) o ohybanemu slovnimu tvaru. Napr. Praze => Praha
  - co je stemmatize - nalezeni kmene slova (Praze -> Prah, protoze koncovka ze => h)
  - [Stemming](https://www.elastic.co/guide/en/elasticsearch/reference/current/stemming.html)
  - [Analyzator hunspell](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-hunspell-tokenfilter.html)

TODO
  - jak na rozsireni hunspell slovniky
    - jak udrzovat slovnik

## Instalace a nastavení

### Stažení slovniků

<div class="grid cards" markdown>
-   :flag_cz: __Čeština__
    
    ---

    :octicons-info-16: Info na [root.cz](https://www.root.cz/zpravicky/nova-verze-ceskeho-slovniku-pro-hunspell/) ze 21.5.2021. Doplněný slovník o nová slova.

    [:material-download: download](http://www.translatoblog.cz/hunspell/)

-   :flag_sk: __Slovenština__

    ---

    :fontawesome-solid-flask: Slovenština nemá pro elasticsearch algoritmický stemmer.

    [:material-download: download](https://github.com/sk-spell/hunspell-sk)
</div>

### Připojení slovniků v docker compose
jak ho pripojit k docker compose
- musi byt v - pridat tedy mounting v docker image
    - config/hunspell/cs_CZ/cs_CZ.dic a 
    - config/hunspell/cs_CZ/cs_CZ.aff

~~~
  volume ./xxx.dic:/usr/share/elasticsearch/config/hunspell/cs_CZ/cs_CZ.dic
  volume ./xxx.aff:/usr/share/elasticsearch/config/hunspell/cs_CZ/cs_CZ.aff
~~~

### ICU folding

  - jde o pluggin
  - dodal jsem magicky command do docker compose (viz docker compose)
