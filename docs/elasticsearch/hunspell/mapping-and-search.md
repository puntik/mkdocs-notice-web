## Algorithmic stemmers 

[language analyzer](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html)

pro češtinu: 

  - stemmer s locale: czech

~~~json
// PUT test-000001
{
  "mapping": {
    "analysis": {
      "filter": {
        "czech_keywords": {
          "type": "keyword_marker",
          "keywords": ["VZP"]
        },
        "czech_stemmer": {
          "type": "stemmer",
          "language": "czech"
        },
        "czech_stop": {
          "type": "stop",
          "stopwords": "_czech_"
        }
      }
    }
  }
}
~~~

  - **keyword_marker** - označí slova, která nebudou analyzována, typicky názvy firem, produktů a pod.
  - **stemmer** - filter pro zpracování
  - **stop** - definuje stop words - slova vyloučena z dalšího zpracoání a indexace. Např. a s k o i u v z. Viz. [github](https://github.com/apache/lucene/blob/main/lucene/analysis/common/src/resources/org/apache/lucene/analysis/cz/stopwords.txt)

pro angličtinu:

  - **stemmer** - english
  - **kstem** - interne stemmer s locale light_english
  - **porter_stemmer** - implementacr Porter stemmer algoritmu
  - **snowball** - jiný stemmer algoritmus pro různé jazyky

## Dictionary stemmers

[hunspell](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-hunspell-tokenfilter.html)

pro cestinu

- hunspell

pro slovenstinu (TODO - pripravit a vyzkouset)

  - hunspel info na [gitlabu](https://github.com/essential-data/hunspell-sk)

## Příklady mappingu a hledání

~~~json
// PUT test-000001
{
  "settings": {
    "analysis": {
      "filter": {
        "nunspell_cs": {
          "type": "hunspell",
          "locale": "cs_CZ",
          "dedup": false
        },
        "czech_keywords": {
          "type": "keyword_marker",
          "keywords": []
        },
        "czech_stemmer": {
          "type": "stemmer",
          "language": "czech"
        },
        "czech_stop": {
          "type": "stop",
          "stopwords": "_czech_"
        }
      },
      "analyzer": {
        "hspl_cs": {
          "tokenizer": "standard",
          "filter": [
            "czech_stop",
            "nunspell_cs"
          ]
        },
        "hspl_cs_lwr": {
          "tokenizer": "standard",
          "filter": [
            "czech_stop",
            "nunspell_cs",
            "lowercase",
            "icu_folding"
          ]
        },
        "stmr_cs": {
          "tokenizer": "standard",
          "filter": [
            "czech_stop",
            "czech_stemmer"
          ]
        },
        "stmr_cs_lwr": {
          "tokenizer": "standard",
          "filter": [
            "czech_stop",
            "czech_stemmer",
            "lowercase",
            "icu_folding"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "title": {
        "type": "text",
        "fields": {
          "hspl_cs": {
            "type": "text",
            "analyzer": "hspl_cs"
          },
          "hspl_cs_lwr": {
            "type": "text",
            "analyzer": "hspl_cs_lwr"
          },
          "stmr_cs": {
            "type": "text",
            "analyzer": "stmr_cs"
          },
          "stmr_cs_lwr": {
            "type": "text",
            "analyzer": "stmr_cs_lwr"
          }
        }
      }
    }
  }
}
~~~

Organizace dat

  - jeden index, kazdy jazyk ma svuj field/property s vlastnim analyzatorem
  - vhodne pro eshop, kde mam preklady nazvu produktu

~~~json
// PUT
{
  "mapping": {
    "properties": {
      "title_cs": {},
      "title_en": {},
    }
  }
}
~~~
nebo 
~~~json
// PUT
{
  "mapping": {
    "properties": {
      "title": {
        "fields": {
            "cs": {},
            "en": {},
        }
      }
    }
  }
}
~~~
  - dva indexy, každý zvlášť pro jednotlivé jazyky

~~~json
// PUT articles-cs
{
  "mapping": {
    "properties": {
      "title": {}
    }
  }
}

// PUT articles-sk
{
  "mapping": {
    "properties": {
      "title": {}
    }
  }
}
~~~

a potom msearch nad oběma indexama
~~~json
// POST _msearch
{"index": "articles-cs"}
{"query" : {"match" : { "title": "this is a test"}}}
{"index": "articles-en"}
{"query" : {"match" : { "title": "this is a test"}}}
~~~

