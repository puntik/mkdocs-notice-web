# Hunspell - zpracování nového slovníku

## Prehled pouziti:

Hunspell je puvodne nastroj pro kontrolu pravopisu, to co my od nej potrebujeme je v podstate opacny postup, nez na
ktery byl urcen.

Elastic si vyrobi mapu, ktera podle nejakych pravidel jako klic udela skloneny/casovany tvar slova a k nemu priradi
prislusny zakladni tvar.

Slabina je v tom, pokud se nektere sklonovane/casovane tvary potkaji a jsou shodne, ale vedou k jinym zakladnim tvarum.
Ale nestava se to nastesti casto.

Hunspell pouziva dva zakladni soubory:
.aff - obsahuje seznam prefixu a suffixu, ktere rikaji, co se se slovem ma delat .dic - to je vlastni seznam zakladnich
tvaru s prefixem/suffixem, kterym se generuji prislusne tvary slov

Existuje nejaky zakladni opensource slovnik (daji se sehnat i jine, napriklad brnenska VUT ma docela dobry, ale placeny)
. Na ten zakladni slovnik bych nesahal, ale pro nova slova bych pouzit slovnik dalsi, nami udrzovany. Novy slovnik musi
byt umisten ve stejnem adresari jako je ten zakladni a prefix soubor - elasticsearch je nastaven na prislusny adresar a
po restartu si ho nacte a zpracuje. Jeste pozor, puvodni slovnik je v ISO kodovani cestiny, je potreba prekonvertovat
treba iconvem na UTF8 nebo neco, v cem je to slovo nakonec indexovani v elastic searchi.

https://www.systutorials.com/docs/linux/man/4-hunspell/

## Struktura souboru .aff

~~~
SFX flag stripping suffix [condition [morphological_fields...]]
~~~

priklady (vsechno pro vzor hrad):

~~~
SFX H g  zích  g          # log => lozích 
SFX H h  zích  [^c]h      # kruh => kruzích 
SFX H ch ších  ch         # melouch => melouších 
SFX H k  cích  k          # krok => krocích
~~~

Jen pro prehled: povel PFX/SFX urcuji, co se se slovem udela:

* SFX: urcuje, ze se neco dela na konci slova (PFX analogicky na zacatku)
* flag: popis - to je to za lomitkem v souboru .dic (P)
* stripping: urcuje, co se ma ze zakladniho slova uriznout, pokud je 0, pripoji se to na konec/zacatek
* suffix: co se pripoji
* condition: podminka, ktera urcuje pouziti pravidla

## Struktura souboru .dic:

Je jednoducha - prvni radek obsahuje cislo s poctem radku, aby se to pri parsovani nemuselo prepocitavat Dalsi radky
obsahuji zakladni tvar slova + za lomitkem postup jak ho zpracovat podle popisu v .aff. Pokud dany radek lomitko
neobsahuje, jde o pozustatek z puvodniho urceni - kontrola pravopisu, kdy editor to slovo zna a neoznacuje ho za chybu,
ale pro nas je to k nicemu - v nasem slovniku by takova situace nastavat nemela.

napr:
moře/M - slovo more bude zpracovano jako moře, moři, mořem, mořím, mořích …

## Prehled zakladnich suffixu pro podstatna jmena:

Sestavil jsem to podle vozru pro prislusny rod podstatnych jmen a jde o zakladni prehled.

### střední rod

|       |     |
|-------|-----|
| město | MQ  |
| moře | M   |
| kuře | K   |
| stavení | SN  |

### ženský rod

|       |     |
|-------|-----|
| žena  | ZQ  |
| růže  | Z   |
| píseň | Z   |
| kost  | K   |

### mužský rod

|          |     |
|----------|-----|
| hrad     | HR  |
| muž      | UV  |
| stroj    | S   |
| pán      | P   |
| předseda | PV  |
| soudce   | UV  |

## Prehled zakladnich suffixu pro pridavna jmena:

|||
|---|---|
| mladý (tvrdý vzor) | YKRN |
| jarní (měkký vzor) | YR |
