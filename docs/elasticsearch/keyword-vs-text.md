Kdy použít typ keyword a kdy text?

## TL;DR
Keyword není analyzovaný, Text je analyzovaný. :slight_smile: 

## Keyword
Typ keyword neni analyzovaný. To znamená, že do indexu se ukládá celý a normalizovaný řetězec tak, jak ho chci mít uložený (normalizací je myšleno napřiklad zbavení diakritiky, převedení na ascii a/nebo lowercase).

!!! note "Jednou větou"
    Odpovědí je ano nebo ne.

Použití typicky pro

* labely
* tagy
* neměnné názvy kategorií
* url
* ip (i když ty ty maji vlastni typ umožňující vyhledávání například podle rozsahů a/nebo masky)

Vyhledávání v nich je většinou odpověď na otázku: obsahuje field xx hodnotu vv - a odpovědí je ano/ne.
Můžu tím tedy získat řešení exact match. Typický dotaz v elastic search je term nebo terms.

### Hledání v kibaně
Občas je potřeba najít něco v logách kibany, tak uvádím stučný přehled využitelný pro jednoduché filtrování. 

Jako příklad je field author.role mapován jako keyword s hodnotou writter.
```json title="Příklad mappingu"
PUT novels
{
    "mapping": {
        "properties": {
            "author": {
                "properties": {
                    "role": {
                        "type": "keyword"
                    }
                }
            }
        }
    }
}
```

```json title="Ukázková hodnota"
POST novels/_doc
{
    "author": {
        "role: "writter"
    }
}
```

#### Syntaxe KQL
Jednoduchý dotazovací jazyk

    author.role: *       => zjistí existenci fieldu v dokumentu
    author.role: writter => přesná shoda
    author.role: writter => bez shody
    author.role: writ*   => shoda (wildcard)

podporovaný je jen operátor *, který plati pro 0 až n nálezů. 
Neměla by být podporována * na začátku slova z výkonnostnich důvodu (lze nastavit konfigurací kibany)

#### Lucene query syntax
Původní Lucene syntax, trochu výkonější, použitý pro query_string.
Umí regulární výrazy s omezenou množinou (více [dokumentace](https://www.elastic.co/guide/en/elasticsearch/reference/current/regexp-syntax.html))

    author.role:/writter/   => matchuje
    author.role:/writ{2}er/ => matchuje

Podporuje fuzziness - přibližné hledání. Šikovné pro překlepy.

    author.role: writtr~ => matchuje writter - defaultní vzdálenost je 2
    author.role: writr~2 => matchuje writter - rozšiřuju vzdálenost pro hledáni

## Text
Typ text je analyzovaný field. 
Znamená to, že se indexují části různě tak, jak potřebuji a jak předpokládám, že budu vyhledávat. 

!!! note "Jednou větou"
    Odpovědí je číselné vyjádření relevance.

Přiklad indexovaného: Příliš žluťoučký kůň úpěl ďábelské ódy.

-  search as you type ... v indexu jsou uložené části optimalizované tak, abych mohl v v indexu vyhledávat postupně od začátku textu. Přiklad: Příliš… (nalezne ukázkový text)
- znám napřiklad vnitřek textu -  v indexu jsou uložené jednotlivé tokeny. Příklad: kůň
- hledám kobylu ... můžu mít zaindexovaná synonyma. Příklad: kobyla -> kůň
- zohlednění skloňování/časování … Příklad: ďábelská óda → ďábelské ódy

Výsledek hledání není odpvěď ano ne, ale nějaká vypočítaná hodnota relevance nalezených dat (zhruba hustota hledaného textu v textu indexovaném). Typickým dotazem v elasticsearch jsou match, multi_match nebo match_phrase.
