Nastavení maximální velikosti uploadovaného souboru.

## Nastavení velikosti uploadu

### PHP
PHP (ve verzi 7.x - ale ve starších verzích to asi bude obdobné) 

Podle použití php je potřeba nastavit pro příslušná prostředí, tzn. php-fpm nebo apache module.
Nastavení je v **php.ini**. Pro php-cli asi nemá smysl nastavovat.

Nastavuje se hodnota **upload_max_filesize**. Více v [manuálu](http://php.net/manual/en/ini.core.php#ini.upload-max-filesize).

Ještě je ještě potřeba nastavit hodnotu  **post_max_size**, s defaultní hodnotou 8M. Měla by být menší něž nastavení **memory_limit** a větší 
než **upload_max_filesize**. Pro neomezenou velikost se nastaví **0**. Další info v [dokumentaci](http://php.net/manual/en/ini.core.php#ini.post-max-size).
 
~~~
# default value - 2M
upload_max_filesize = 80M

# default value - 8M
post_max_size = 100M
~~~

### Nginx

Dále je ptřeba nastavit i velikost souboru, který povolí nginx do dalšího zpracování.
Nastavení pro celý server, tzn. všechny virtual hosty. Nastavení v proměnné client_max_body_size v konfiguraci **nginx.conf** 
nebo sites-available/server.conf pro virtual host. Příslušná [dokumentace](http://nginx.org/en/docs/http/ngx_http_core_module.html#client_max_body_size)

~~~
http {
	#...
	# default value 1m - 1 MB
    	client_max_body_size 100m;
	#...
}
~~~

Hodnotu je možné nastavit jen u jednotlivého virtual hostu.

~~~
server {
	#...
    	client_max_body_size 100m;
	#...
}
~~~

### Apache
Obdobně i Apache HTTP. Opět je možné proměnné nastavit globálně pro celý server nebo jen pro virtual host. 
Nastavení v proměnné **LimitRequestBody**, hodnota se udává v bytech, maximální hodnota je 2GB.
Podrobněji v [dokumentaci](https://httpd.apache.org/docs/2.4/mod/core.html#limitrequestbody).

~~~
# Default 0 - unlimited
<Directory "/var/www/virtuals/rem.cz/public/">
    LimitRequestBody 102400 
</Directory>
~~~

## Timeout

### PHP

PHP má limit provádění skriptu. Nastaven je v proměnné **max_execution_time**, integer hodnota udávaná v sekundách. Skripty 
spouštěné z příkazové řádky mají timeout nastavení na neomezenou dobu. Další informace v [dokumentaci](http://php.net/manual/en/info.configuration.php#ini.max-execution-time).

~~~
# nastavení na 3 minuty.
max_execution_time 180
~~~

A jen pro jistotu - skripty prováděné supervisorem jsou brány jako php-cli, tak běží bez timeoutu.

### Nginx
Je potřeba nastavit proměnnou **proxy_read_timeout** (defaultně 60s), která definuje délku čekání na proxovaný server (v našem 
přípagě php-fpm). Více [info](http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_read_timeout).

~~~
proxy_read_timeout 180s;
~~~

### Apache
Podobně v Apache, kde je pro to directiva **TimeOut**, podrobnosti v [dokumentaci](https://httpd.apache.org/docs/2.4/mod/core.html#timeout). 
Nastavuje se na úrovní serveru nebo virtual hostu. 

~~~
Timeout 300
~~~
