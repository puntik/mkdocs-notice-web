## 2xx - reportují úspěšné provedení requestu

### 200 - ok
**GET**, **POST** nebo **PUT** - indikuje úspěšné nalzení entity. 
V těle odpovědi je aktuální stav nalezené entity.

### 201 - created
Používáme v případech **POST** a/nebo **PUT** při vytváření nové entity. 
V těle odpovědi dáváme aktualní stav nově vytvořené entity.

### 202 - accepted
Moc nepoužíváme, ale obecně je používán v případě **POST**, **PUT**, **PATCH** nebo **DELETE**, když server odloží provedení 
akce do fronty. Do těla odpovědi vkládáme identifikátor zprávy ve frontě, v připadě potřeby je tedy možné zjistit stav zpracování.

### 204 - empty response
Používáme v případě úspěšného provedení **DELETE**. Tělo zprávy je prázdné.

## 4xx - chyby na straně klienta

### 400 - bad request
Obecná chyba ze strany klienta, kdy data prošla validací (jinak vracíme 422), ale z nějakého důvodu server nemůže požadavek
splnit. V těle zprávy je obsažen textový popis chyby nebo aplikační domluvený kód chyby a textový popis.

```json
{
	"code" : 1001,
	"message" : "Invalid time to start processing."
}
``` 

### 401 - unauthorized
Vráceno klientovi v situaci, kdy nemá přístup ke zdrojům, typicky uživatelské jméno, heslo, roli a příslušná práva ke zdrojům.

### 403 - forbidden
Vráceno v případě pokusu o přístup k datům, ke kterým přihlášený uživatel nemá oprávnění, například pokus o čtení dat 
cizího uživatele. Někdy se z bezpečnostních důvodů uvažuje místo vrácení _403 FORBIDDEN_ vrátit kód _404 NOT FOUND_.

### 404 - not found
Vracíme, pokud není nalezena požadovaná entita, typicky neexistující id nebo jiný primární klíč.

### 409 - conflict
Používáme zřídka pro situace, kdy uživatel požaduje změnu stavu entity tak, jak to není možné. Příklad může být pokus nastavit 
stav tasku na vyřešený bez průchodu stave review. 
 
### 422 - unprocessable entity
Používáme pro chybu validace vstupních dat (v Laravelu ValidationException). V těle je uveden popis chyb jednotlivých vstupů
tak, aby bylo možné je použít pro handlování chyb ve formuláři.

```json
{
	"email": "Field is not valid email",
	"send_at": "Field is not valid time in future"
}
```

## 5xx - Chyby na straně klienta

Všechny chyby 5xx jsou na straně serveru. Klient by na ně také měl adekvátně reagovat. Nicméně jejich popis je mimo rozsah 
a účel tohoto dokumentu.

* _500 - internal server error_ - téměř cokoliv, je dobré se podívat do logu.
* _501 - not implemented_ - vracíme v případě, kdy máme připravený mock pro testování, ale klient se na zdroj zeptá produkce.
V tom prípadě se mockovaná data nemůžou a místo nich feature toggle vrátí informaci o neimplementované funkci.
* _502 - bad gateway_ - zpracovatel requestu není dostupný: v našem případě nginx nedostane zpátky info od php-fpm nebo node.js.
* _504 - gateway timeout_ - podobná situace jako 502 - rozdíl je v tom, ze zpracovatel requestu dovolí připojení, ale nevrátí
odpověď.


## Zdroje a linky, dalsi cetba ...

* [REST API Error Codes 101](https://blog.restcase.com/rest-api-error-codes-101/) - ukázka řešení na velkých serverech typu Facebook
* [HTTP Status 202 (Accepted)](https://restfulapi.net/http-status-202-accepted/) - reprezentace dat fronty
* [Redirecting to 404 from 403](https://geekflare.com/404-instead-of-403/) - přesměrování 403 na 404


