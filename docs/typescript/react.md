## Install all parts

### Install yarn

~~~
npm install -g yarn
~~~

### Install react

[Documentation](https://vitejs.dev/guide/)

~~~shell
yarn create vite my-vue-app --template react-ts

cd my-vue-app

yarn

yarn dev --host
~~~

### Install taiwindcss

[Documentation](https://tailwindcss.com/docs/installation)

~~~shell
yarn add tailwindcss postcss autoprefixer
npx tailwindcss init -p --ts
~~~

—ts … install typescript config for tailwindcss
-p … create postcss

~~~typescript title="tailwind.config.js" hl_lines="5 6"
import type {Config} from 'tailwindcss'

export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {},
    plugins: [],
} satisfies Config
~~~

set src/index.css

~~~css title="src/index.css"
@tailwind base;
@tailwind components;
@tailwind utilities;
~~~

run:

~~~shell
mkdir dist/
npx tailwindcss -i ./src/index.css -o ./dist/output.css --watch
~~~

pycharm/phpstorm should work automatically, no manual settings needed

### Install taiwindcss plugins

~~~
yarn add @tailwindcss/forms
~~~

~~~typescript title="tailwind.config.js" hl_lines="3 12"
import type {Config} from 'tailwindcss'

import generated from "@tailwindcss/forms";

export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {},
    plugins: [
        generated,
    ],
} satisfies Config
~~~

### Install flowbite

- [Flowbite quickstart](https://flowbite.com/docs/getting-started/quickstart/)
- [Flowbite react](https://flowbite.com/docs/getting-started/react/)

~~~shell
yarn add flowbite flowbite-react
~~~

~~~typescript title="tailwind.config.js" hl_lines="3 10 16"
import type {Config} from 'tailwindcss'

import flowbite from "flowbite-react/tailwind"
import generated from '@tailwindcss/forms'

export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
        flowbite.content(),
    ],
    theme: {
        extend: {},
    },
    plugins: [
        flowbite.plugin(),
        generated
    ],
} satisfies Config
~~~

And finally, setup dark mode:
~~~typescript title="tailwind.config.js" hl_lines="19"
import type {Config} from 'tailwindcss'

import flowbite from "flowbite-react/tailwind"
import generated from '@tailwindcss/forms'

export default {
    content: [
        "./index.html",
        "./src/**/*.{js,ts,jsx,tsx}",
        flowbite.content(),
    ],
    theme: {
        extend: {},
    },
    plugins: [
        flowbite.plugin(),
        generated
    ],
    darkMode: 'media'
} satisfies Config
~~~
or more compicated
~~~tsx title="index.tsx"
"use client"

import './index.css'
import {DarkThemeToggle, Flowbite} from "flowbite-react"

function App() {
    return (
        <Flowbite>
            <main className="flex min-h-screen items-center justify-center gap-2 dark:bg-gray-800">
                <h1 className="text-2xl dark:text-white">Flowbite React + Vite</h1>
                <DarkThemeToggle/>
            </main>
        </Flowbite>
    );
}

export default App;
~~~


## Notes on production

~~~
yarn build

yarn add serve
  
serve -s build
~~~ 

## Some useful react libraries:

~~~shell
yarn add formik
yarn add react-select
yarn add react-icons

yarn add react-router - page routing
~~~




