# Instalace typescriptu

Rozjeti projektu, instalace typescriptu. Predpokladem je uz nainstalovany node a yarn.

## Typescript pro jednoduchou aplikaci
~~~bash
mkdir -p ${PROJECT_NAME}/src ${PROJECT_NAME}/dist
cd ${PROJECT_NAME}
~~~

~~~bash
yarn init -y && yarn add dotenv typescript @types/node
~~~

~~~bash
npx tsc --init
~~~

Vznikne tsconfig.ten editovat, hlavne nastavit outDir: "dist/". Asi takhle nejak: 
~~~json
{
    "outDir": "dist/"
}
~~~

V adresari src/ vyrobit index.ts a prvni skript:
~~~
cat >> src/index.ts
console.log("Hello world")
~~~

No a je to - zbuildovat:
~~~
npx tsc
~~~

a spustit
~~~ 
node dist/index.js
~~~

Sikovne je si to ulozit do packages.json 
~~~json
{
    "scripts": {
        "build": "npx tsc",
        "start": "node dist/index.js",
    }
}
~~~

a pak to poustet pomoci yarn
~~~bash
yarn build
yarn start
~~~