
## Services

Example of elastic computed cloud (EC2) instances and use cases.

| type   | procs | mem [GB]| network [Gbps]  | storage  | notice               |
|--------|-------|---------|---------------- |----------|----------------------|
|t2.micro| 1     | 1       | Low to Moderate | EBS-Only | free tier applicable |
|t3.micro| 2     | 1       | Up to 5         | EBS-Only |                      |
|t2.large| 2     | 8       | Low to Moderate | EBS-Only |                      |
|t3.large| 2     | 8       | Up to 5         | EBS-Only |                      |
|r5.large| 2     | 16      | Up to 10        | EBS-Only | memory optimized     |
|c5.large| 2     | 4       | Up to 10        | EBS-Only | compute optimized    |

* **t2** - websites and web applications, development environments, micro services, test and staging environments
* **t3** - micro-services, low-latency interactive applications, development environments and business-critical applications 
* **r5** - high performance databases, real time big data analytics and other enterprise applications.
* **c5** - high performance web servers, scientific modelling, batch processing, distributed analytics, machine/deep learning inference, ad serving


## Cluster pricing

### Test environment

* elastic compute cloud (_t2.micro_)
* then installnginx, for persistence use sqlite
* useful only for testing applications, no for production environment

| service       |  hourly | montly | notice                             | 
|---------------|---------|--------|------------------------------------|
| ec2 t2.micro  | $0.0134 | $9.97  | 750 computed hours is in free tier | 

### Common production environment (minimal setup) 

* elastic compute cloud with nginx and supervisord (_t2.micro_)
* rds (_db.r2.micro_)
* elastic cache (_cache.t2.micro_)
* simple queue service (SQS)

| service           |  hourly | montly | notice                                             | 
|-------------------|---------|--------|----------------------------------------------------|
| ec2 t2.micro      | $0.0134 |  $9.97 | 750 hours is in free tier                          | 
| rds db.t2.micro   | $0.02   | $14.88 | 750 hours is in free tier                          | 
| ec cache.t2.micro | $0.02   | $14.88 | 750 hours is in free tier                          |
| SQS               | $0.4    |        | price is for 1M requests, first 1M apply free tier | 

### Clustered production environment

* elastic compute cloud with nginx and supervisord
* rds
* elastic cache 
* simple queue service (SQS)

TBD

## Configuration examples

Single server is suitable for testing purpose only. Data are stored in file database (instead of RDS), queuing is turned 
off and data are processed synchronously. There is no cache, or it uses local file cache.

![simple](../../_images/aws_single.png)

Simple cluster for production, low/mid traffic environment. It uses RDS, caching and data queuing as service. EC2 contains 
web server for serving content and supervisord for asynchronous processing data.

![simple](../../_images/aws_small_cluster.png)

High availability architecture.

![simple](../../_images/aws_big_cluster.png)

## Notice

* All prices are for Frankfurt (region EU1).
* Free tier is applicable for first year of service usage.
* Prices on invoices can be different - check actual service in aws stack.
* Prices do not include traffic fees and taxes.
* Prices are _on demand_, _reserved resources_ can decrease total amount of invoice :)
 
