# EC2 for running php

## install php

=== "8.1"

    ~~~shell
    # update instance
    sudo apt-get update && sudo apt-get upgrade
    
    # install php
    sudo add-apt-repository ppa:ondrej/php
    sudo add-apt-repository ppa:ondrej/nginx-mainline
    sudo apt-get install php8.1 php8.1-cli php8.1-fpm
    sudo apt-get install php8.1-curl php8.1-mbstring php8.1-zip php8.1-xml php8.1-bcmath php8.1-intl
    sudo apt-get install php8.1-sqlite3 php8.1-mysql php8.1-gd
    sudo apt-get install php-pear php-redis
    ~~~

=== "8.0"

    ~~~shell
    # update instance
    sudo apt-get update && sudo apt-get upgrade
    
    # install php
    sudo add-apt-repository ppa:ondrej/php
    sudo add-apt-repository ppa:ondrej/nginx-mainline
    sudo apt-get install php8.0 php8.0-cli php8.0-fpm
    sudo apt-get install php8.0-curl php8.0-mbstring php8.0-zip php8.0-xml php8.0-bcmath php8.0-intl
    sudo apt-get install php8.0-sqlite3 php8.0-mysql php8.0-gd
    sudo apt-get install php-pear php-redis
    ~~~

=== "7.4"

    ~~~shell
    # update instance
    sudo apt-get update && sudo apt-get upgrade
    
    # install php
    sudo add-apt-repository ppa:ondrej/php
    sudo add-apt-repository ppa:ondrej/nginx-mainline
    sudo apt-get install php7.4 php7.4-cli php7.4-fpm
    sudo apt-get install php7.4-curl php7.4-mbstring php7.4-zip php7.4-xml php7.4-bcmath php7.4-intl
    sudo apt-get install php7.4-sqlite3 php7.4-mysql php7.4-gd
    sudo apt-get install php-pear php-redis
    ~~~

Next software and services

~~~bash
# install chrony 
sudo apt install chrony

# install nginx 
sudo apt-get install nginx

# install sqlite
sudo apt install sqlite3

# install supervizord
sudo apt-get install supervisor
~~~

## Configure chrony

[more info](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/set-time.html#configure-amazon-time-service-ubuntu)

Info config file **/etc/chrony/chrony.conf** add the following line before any other server or pool:

    server 169.254.169.123 prefer iburst minpoll 4 maxpoll 4

And then restart

~~~shell
sudo service chrony restart
~~~

## Configure php and nginx

TBD

## Start services and/or run on start up

~~~bash
# start services
sudo service php8.0-fpm start
sudo service nginx start
sudo service chrony restart
sudo service supervizord start

# run on start up
sudo update-rc.d php8.0-fpm defaults
sudo update-rc.d nginx defaults
sudo update-rc.d chrony defaults
sudo update-rc.d supervizord defaults
~~~
