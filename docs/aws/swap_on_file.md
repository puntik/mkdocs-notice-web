# Create swap file on aws

## Some basic commands:

~~~
sudo swapon --show
free -h
df -h
~~~

| Amount of RAM in the system| Recommended swap space     | Recommended swap space if allowing for hibernation |
|----------------------------|----------------------------|----------------------------------------------------|
| 2GB of RAM or less         | 2 times the amount of RAM  | 3 times the amount of RAM                          |
| 2GB to 8GB of RAM          | Equal to the amount of RAM | 2 times the amount of RAM                          |
| 8GB to 64GB of RAM         | At least 4 GB              | 1.5 times the amount of RAM                        |
| 64GB of RAM or more        | At least 4 GB              | Hibernation not recommended                        |

~~~
sudo fallocate -l 1G /swapfile
ls -lh /swapfile

sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
~~~

jako veriantu jak vytvorit swap partisnu na aws (store volumes) nebo pomoci fdisk oddelit vlastni partisnu

Make the swap file permanent

~~~
sudo cp /etc/fstab /etc/fstab.bak
echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
~~~

## Swap config improvements

~~~
cat /proc/sys/vm/swappiness
~~~

Tohle je 0-100 - 0 neswapuje nic, 100 swapuje vsechno - je dobre najit kompromis.
Nizsi cisla jsou vhodna pro server (swap ve file je pomalejsi nez ziva pamet)
Vyssi cisla pro desktop

~~~
sudo sysctl vm.swappiness=10
sudo nano /etc/sysctl.conf
~~~

nastavit vm.swappiness = 10


Tohle se tyka cachovani adresaru a indodu souboru. 100 je defaultni hodnota, 
0 necachue info o otevrenych souborech - muze vest k zaplneni pameti. 
vice ne 100 (napr. 1000) - kernel cachuje vsechno a casto - vykonostni problem vzhledem k pomalejsi cache nez pameti.

~~~
cat /proc/sys/vm/vfs_cache_pressure
sudo sysctl vm.vfs_cache_pressure=50
sudo nano /etc/sysctl.conf
~~~

nastavit vm.vfs_cache_pressure = 50 

## Další čtení

- [kernel](https://www.kernel.org/doc/Documentation/sysctl/vm.txt)
- [askubuntu](https://askubuntu.com/questions/49109/i-have-16gb-ram-do-i-need-32gb-swap)
- [ldp](https://www.tldp.org/HOWTO/Partition/setting_up_swap.html)



