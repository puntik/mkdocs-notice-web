## EKS price table

|                        | cluster | 
|------------------------|---------|
| ap-norhteast-1 (hour)  |   $0.10 |  
| ap-norhteast-1 (month) |  $73.00 |  
| eu-central-1 (hour)    |   $0.10 | 
| eu-central-1 (month)   |  $73.00 |   

## EC2 price table (nodes)

|                        | t3.nano | t3.micro | t3.large | t3.xlarge | t4a.xlarge |
|------------------------|---------|----------|----------|-----------|------------|
| vCPU                   |       2 |        2 |        2 |         4 |          4 |
| Mem                    |     0.5 |        1 |        8 |        16 |         16 |
| ap-norhteast-1 (hour)  | $0.0068 |  $0.0136 |  $0.1088 |   $0.2176 |    $0.1728 |
| ap-norhteast-1 (month) |  $4.964 |   $9.928 |  $79.424 |  $158.848 |   $126.155 |
| eu-central-1 (hour)    |  $0.006 |   $0.012 |   $0.096 |    $0.192 |    $0.1536 |
| eu-central-1 (month)   |   $4.48 |    $8.76 |   $70.08 |  $140.160 |   $112.128 |

## Network

TBD

## Sources 
- [EKS pricing](https://aws.amazon.com/eks/pricing/)
- [EC2 On demand price list](https://aws.amazon.com/ec2/pricing/on-demand/)