# Snippety pro vscode

Seznam snippet pro tvorbu kubernetes manifestů. 

Link ke [stažení](https://bitbucket.org/puntik/workspace/snippets/Lze9Ej) z bitbucket.

!!! info "Instalace na mac OS"

    V menu **Code->Settings->Configure User Snippets** vyrobit nebo vybrat soubor se snippety.

## Pro celé šablony

- ingress-vk
- service-vk
- deployment-vk
- config-map-vk
- job-vk
- crobjob-vk
- pv-vk
- pvc-vk

## Inline šablony

- volumes-vk
- volume-mount-vk
  