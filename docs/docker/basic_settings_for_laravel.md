## Adminer mysql

~~~yaml title="docker-compose.yaml"
services:
    adminer:
        image: adminer
        container_name: adminer
        restart: always
        ports:
            - 8080:8080
        networks:
            - app-network
~~~

spustit na [http://localhost:8080](http://localhost:8080)

Přihlášení do admineru:

* server: **db** _(podle názvu service kde běží databáze, v našem případě tedy db)_
* username: **homestead** _(proměnné prostředí MYSQL_USER)_
* password: **secret** _(proměnné prostředí MYSQL_PASSWORD)_
* database: **homestead** _(proměnné prostředí MYSQL_DATABASE)_

## Adminer sqlite

~~~yaml title="docker-compose.yaml"
  adminer:
    image: adminer
    container_name: nutrio-adminer
    restart: unless-stopped
    ports:
      - "8080:8080"
    volumes:
      - "./database:/data:rw"
      - ./.docker-config/adminer/login-password-less.php:/var/www/html/plugins-enabled/login-password-less.php
    environment:
      ADMINER_PLUGINS: "tables-filter"
    networks:
      - app-network
~~~

Poznámky:

Stáhnout a do projektu nakopírovat soubor [login-password-less.php](https://github.com/vrana/adminer/blob/master/plugins/login-password-less.php), protože sql nepodporuje přihlášení heslem, zatimco adminer ho bez plugginu vyžaduje.

~~~php title="login-password-less.php"
<?php
require_once('plugins/login-password-less.php');

return new AdminerLoginPasswordLess(
    $password_hash = password_hash("admin", PASSWORD_DEFAULT)
);
~~~

Dál propojit soubor s databází do containeru - adresář **/database**.

* server: 
* username: **admin**
* password: **admin**
* database: **/data/database.sqlite**

~~~yaml title=".env"
DB_CONNECTION=sqlite
DB_HOST=database.sqlite # defaultni jméno databáze
~~~

## Mysql

~~~yaml title="docker-compose.yml"
services:
    db:
        image: mysql
        container_name: db
        command: --default-authentication-plugin=mysql_native_password
        restart: always
        ports:
            - 3306:3306
        environment:
            MYSQL_DATABASE: homestead
            MYSQL_USER: homestead
            MYSQL_PASSWORD: secret
            MYSQL_ROOT_PASSWORD: secret
        volumes:
            - db_data:/var/lib/mysql
        networks:
            - app-network

volumes:
    db_data:
        driver: local
~~~

~~~dotenv  title=".env"
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
DB_CHARSET=utf8
DB_COLLATION=utf8_czech_ci
DB_TIMEZONE=+01:00
~~~

## Mailhog

~~~yaml title="docker-compose.yml"
services:
    mailhog:
        image: mailhog/mailhog
        container_name: mailhog
        ports:
            - 1025:1025
            - 8025:8025
        networks:
            - app-network
~~~

~~~dotenv  title=".env"
MAIL_DRIVER=smtp
MAIL_HOST=localhost
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=admin@localhost
MAIL_FROM_NAME="${APP_NAME}"
~~~

spustit na [http://localhost:8025](http://localhost:8025)

_pozn. teoreticky neni potreba zverejnit port 1025, je viditelny pouze z vnitrni site, ale jisty si tim nejsem :D_

## MinIO (nahrazeni AWS S3)

~~~yaml title="docker-compose.yml"
services:
    minio:
        image: minio/minio:latest
        container_name: minio
        restart: always
        environment:
            MINIO_ACCESS_KEY: homestead
            MINIO_SECRET_KEY: secretsecret
            MINIO_DEFAULT_BUCKETS: data,users
        ports:
            - 9000:9000
        volumes:
            - minio_data:/data
        command: server /data
        networks:
            - app-network

volumes:
    minio_data:
        driver: local
~~~

### Uprava konfigurace filesystems.php

Podobné nastavení jako normální AWS S3.

~~~php
'minio' => [
    'driver' => 's3',
    'key'    => env('AWS_ACCESS_KEY_ID'),
    'secret' => env('AWS_SECRET_ACCESS_KEY'),
    'region' => env('AWS_DEFAULT_REGION'),
    'bucket' => env('AWS_BUCKET'),
    'url'    => env('AWS_URL'),

    'endpoint'                => env('AWS_URL'),
    'use_path_style_endpoint' => true,
],
~~~

~~~dotenv  title=".env"
AWS_ACCESS_KEY_ID=homestead
AWS_SECRET_ACCESS_KEY=secretsecret
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_URL=http://localhost:9600
~~~

spustit na [http://localhost:9000](http://localhost:9000)


