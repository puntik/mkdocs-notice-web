MicroK8s is a small, fast, single-package Kubernetes for developers, IoT and edge devices. It's great for offline development, prototyping, testing, and other scenarios. In this article, we'll go through the steps of installing MicroK8s on an Ubuntu system.

## Prerequisites

For the purpose of following this guide, you will need:

- An Ubuntu system
- Admin access to perform commands with `sudo`

## Installation Steps

### Step 1: Update Ubuntu

Ensure your Ubuntu system is up-to-date by running the following apt command:
```bash
sudo apt update
```
This command fetches the package list from the repositories and "updates" them to get information about the newest versions of packages and their dependencies.

### Step 2: Clean Older Installation (Optional)

If you had MicroK8s installed previously and want to start fresh, you can remove the old installation using Snap:
```bash
sudo snap remove microk8s
```
This command will cleanly remove any old versions of MicroK8s from your system.

### Step 3: Install MicroK8s

Next, install MicroK8s on Ubuntu:
```bash
sudo snap install microk8s --classic
```
The `--classic` option allows MicroK8s to access all necessary resources from the host machine, which are essential for orchestration capabilities.

Alternatively, if you want to use a specific version or channel, you can specify it as follows:
```bash
sudo snap install microk8s --classic --channel=1.30
```

### Step 4: Verify Installation

Once the installation completes, you can check if MicroK8s is part of the Snap applications list:
```bash
sudo snap list
```
This command should display MicroK8s in the output, indicating that it has been successfully installed.

### Step 5: Enable MicroK8s Add-ons

MicroK8s comes with a set of add-ons which can enhance its functionality. We'll enable a few of them:

```bash
microk8s enable ingress
microk8s enable hostpath-storage
microk8s enable observability
microk8s enable metrics-server
```

If you want a Docker registry or MinIO for object storage, you can enable them as follows:

```bash
microk8s enable registry
microk8s enable minio
```

### Step 6: Generate Kube Config File

Finally, generate a `.kube/config` file:

```bash
microk8s config > .kube/config
```

This file is used by `kubectl` to interact with your Kubernetes cluster, enabling you to manage the resources in your cluster effectively.

And that's it! You now have MicroK8s installed on your Ubuntu system. For more details and further steps, refer to the official [MicroK8s documentation](https://microk8s.io/docs/). Happy container orchestrating!

