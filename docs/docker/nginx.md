##Settings nginx with docker
  
docker-compose.yml

~~~yaml

webserver:
    image: nginx:alpine
    container_name: webserver
    restart: unless-stopped
    tty: true
    ports:
        - "80:80"
        - "443:443"
    volumes:
        - ./:/var/www/api
        - ./nginx/conf.d/:/etc/nginx/conf.d/
    networks:
        - app-network

~~~
## Konfigurace nginx.conf a php-fpm

### Zajištění SSL

```shell
certbot certonly --standalone -d {DOMAIN} --agree-tos -m {EMAIL}
```

!!! Success "Poznámka"

    Certifikáty se vygenerují v adresáři **/etc/letsencrypt/live/{DOMAIN}/**

!!! Warning "Standalone"
    - Pro **--standalone** musim vypnout na chvili webserver, potrebuje to k zivotu port 80, 
    na který se připojuje ověřovací letsencrtypt server.
    - **TODO** - overit automaticke generovani ssl certifikatu pro subdomeny s wildcard

### Konfigurace a reload nginx serveru vcetne SSL

~~~
server {
    listen 443 ssl http2;

    ssl_certificate /etc/letsencrypt/live/{DOMAIN}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/{DOMAIN}/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/{DOMAIN}/fullchain.pem;

    server_name {DOMAIN};
    root /var/www/{DOMAIN};

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";

    # access_log /var/log/nginx/nginx.vhost.access.log;
    # error_log /var/log/nginx/nginx.vhost.error.log;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";

    index index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        # fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}

# redirect traffic on port 80
server {
    listen 80;
    server_name {DOMAIN};

    if ($host = {DOMAIN}) {
        return 301 https://$host$request_uri;
    } 

    return 404;
}
~~~

### Konfigurace a reload nginx serveru bez SSL

```
server {
    listen 80;

    server_name {DOMAIN};
    root /var/www/{DOMAIN};

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";

    # access_log /var/log/nginx/nginx.vhost.access.log;
    # error_log /var/log/nginx/nginx.vhost.error.log;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";

    index index.php;

    charset utf-8;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        # fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_pass 127.0.0.1:9000;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}



```


Reloadovat nginx (gentoo version :)

```shell
rc-service nginx reload 
```


