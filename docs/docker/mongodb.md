Redis

~~~yaml

services:
  redis:
    container_name: redis-smartcrypto
    image: redis
    ports:
      - "6379:6379"
    networks:
      - redis-net

  redisinsight:
    container_name: redis-smartcrypto-redisinsight
    image: redis/redisinsight:latest
    ports:
      - "5540:5540"
    volumes:
      - redisinsight:/db
    depends_on:
      - redis
    networks:
      - redis-net

networks:
  redis-net:
    driver: bridge

~~~

MongoDb
~~~yaml

services:
  mongodb:
    container_name: mongodb
    #image: mongodb/mongodb-community-server:latest
    image: mongo
    ports:
      - "27017:27017"
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=secret
      - MONGO_INITDB_DATABASE=project
    networks:
      - mongo-net

  mongo-express:
    container_name: mongo-express
    image: mongo-express
    ports:
      - "8081:8081"
    environment:
      - ME_CONFIG_MONGODB_SERVER=mongodb
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=secret
      - ME_CONFIG_BASICAUTH_USERNAME=admin
      - ME_CONFIG_BASICAUTH_PASSWORD=secret
    restart: always
    depends_on:
      - mongodb
    networks:
      - mongo-net

networks:
  mongo-net:
    driver: bridge
    
~~~