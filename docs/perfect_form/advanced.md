## Bindování modelu v routeru

Často po routování a zpracování v akci formuláře potřebujeme načíst vlastní entitu z předaného id. Laravel sám dokáže načíst 
správnou entitu už během routovaní a předat jí do akce controlleru. 

Náš případ s kontaktem by tedy mohl vypadat následovně: napřed definujeme routu v souboru  __web.php__.

~~~php
<?php

$router->get('/contacts/{contact}/update', 'ContactController@edit');
$router->post('/contacts/{contact}/update', 'ContactController@update');
$router->get('/contacts/{contact}/destroy', 'ContactController@destroy');
~~~

V __RouterServiceProvider__ model zaregistrujeme.
	
~~~php
<?php

public function boot()
{
	Route::model('contact', Contact::class);

	parent::boot();
}
~~~

Do akce kontroleru formuláře bindovanou entitu zavedeme jednoduše __injectováním__. 

~~~php
<?php

class ContactController extends Controller {

    public function index() {
        $contacts = Contact::all();

        return view('contact.index', compact('contacts'));
    }

    public function create() {

        return view('contact.create');
    }

    public function store(ContactRequest $request) {
        $validated = $request->validated();

        Contact::create($validated);

        return view('contact.index');
    }
    
    public function edit(Contact $contact) {
    	return view('contact.edit', [
    		'contact' => $contact,
    	]);
    }
    
    public function update(ContactRequest $request, Contact $contact) {
    	$validated = $request->validated();
    	$contact->update($validated);
    
    	return view('contact.index');
    }
    
    public function destroy(Contact $contact) {
    	$contact->delete();
    
    	return view('contact.index');
    }
}
~~~
