### Refaktorizace a zpřehlednění kódu Controlleru

Laravel obsahuje implementaci __FormRequest__, který můžeme využít pro převzetí validační logiky. Implemetuje metodu 
__rules__, která vrací pole s validačními pravidly.

~~~php
<?php

class ContactRequest extends FormRequest
{

	public function rules()
	{
		return [
			'first_name' => 'required',
			'last_name'  => 'required',
			'email'      => 'required|email'
		];
	}
}
~~~

V kontrolleru pak převezme kompletní validační logiku. Výsledný kód kontroleru je pak přehlednější a odpovědnost za validace
je uklizená v třídě pro to určené.

~~~php
<?php

class ContactController extends Controller {
	
	public function index() {
		$contacts = Contact::all();
		
		return view('contact.index', compact('contacts'));
	}
	
	public function create() {

		return view('contact.create');
	}
	
	public function store(ContactRequest $request) {
		$validated = $request->validated();
	
		Contact::create($validated);
		
		return view('contact.index');
	}
}
~~~
