### Úvod a základní předpoklady

V této kapitole se naučíme připravit základní verzi formuláře. Výsledkem bude tedy funkční controller, view s jednoduchým
formulářem a nastavené routování.

Uvažujeme následující model __Contact__. Protože jde o popis tvorby formuláře, nepotřebujeme zatím žádné relace, zavedeme 
je až později při popisu autorizace. Stejně tak vynecháme tvorbu factory i popis entity a migrace, podrobnosti jsou v 
[dokumentaci Laravelu](https://laravel.com/docs/5.6/eloquent).

~~~php
<?php

class Contact extends Model {

	protected $fillable = [
		'first_name',
		'last_name',
		'email'
	];
}
~~~

s migrací
 
~~~php
<?php

Schema::create('contacts', function (Blueprint $table) {
	$table->increments('id');
	$table->string('first_name', 64);
	$table->string('last_name', 64);
	$table->string('email', 64);
	$table->softDeletes();
	$table->timestamps();
});
~~~

Po úspěčné migraci se v databázi objeví nová tabulka __contacts__.

### Nejjednoduší funkční formulář

Začněme tím nejjednodušším, formulářem pro vytvoření entity. Napřed potřebujeme routování, aby laravel věděl, co má vlastně
dělat. 

Do souboru __routes/web.php__ vložíme následující kód, kterým frameworku řekneme, jaké akce controlleru má volat při jakém 
requestu. Pro jednotlivé akce dodržujeme následující schéma:

|             |              |     					   					   					   					   		   |
| ----------- | -----------  | -----------                                                                                 |
| GET         | index        | zobrazí seznam entit v databázi 					   					   					   |
| GET         | create       | zobrazí formulář pro zadání nových dat do databáze 					   					   |
| POST        | store        | zpracuje formulář, vloží nové data do databáze a zobrazí zprávu o úspěchu 				   | 
| GET         | edit         | zobrazí formulář pro editaci již existujících dat                    			           |  
| POST or PUT | {id}/update  | zpracuje formulář, uloží data do databáze (nebo pošle do fronty) a zobrazí zprávu o úspěchu | 
| DELETE      | {id}/destroy | smaže data 																				   | 

Routování formuláře pro vytvoření entity bude vypadat tedy takto. Opět vynechávám věci nesouvisející přímo s formulářem, 
jako jsou nastavení namespaců, prefixů a pod.

```php
<?php

/*
 * Seznam Contact entit
 */
$router->get('/contacts', 'ContactController@index');

/*
 * Zobrazí formulář pro vytvoření entity
 */
$router->get('/contacts/create', 'ContactController@create');

/*
 * Zpracuje formulář metodou POST
 */
$router->post('/contacts/create', 'ContactController@store');
```

Nyní vytvoříme vlastní ContactController, který zpracovává metody z uvedeného routování.

```php
<?php
 
class ContactController extends Controller {
	
	/*
	 * Akce pro výpis všech entit modelu
	 */
	public function index() {
		$contact = Contact::all();
		
		return view('contact.index', compact('contact'));
	}
	
	/*
	 * Zobrazí formulář
	 */
	public function create() {

		return view('contact.create')
	}
	
	/*
	 * Napojen na akci POST a uloží informace z formuláře
	 */
	public function store(Request $request) {
	
		/*
		 * Data z requestu potřebná pro vytvoření kontaktu
		 */
		$data = $request->only([
			'first_name',
			'last_name',
			'email',
		]);
	
		Contact::create($data); 
		
		return view('contact.index');
	}
}
```

A vlastní formulář nakonec. 
```html
<form method="post">
   {{ csfr_field() }}
   <label for="first_name">First name</label>
   <input name="first_name" id="first_name" type="text" />
   
   <label for="last_name">Last name</label>
   <input name="last_name" id="last_name" type="text"/>

   <label for="email">Email</label>
   <input name="email" id="email" type="text"/>
   
   <input type="submit" name="save_data" value="Save data"/>
</form>
```

### Proč potřebujeme csrf_field a jak to funguje
CSRF (__Cross-site request forgery__) je jeden z typů útoku na [webové stránky](https://cs.wikipedia.org/wiki/Cross-site_request_forgery). 
Spočívá v neautorizovaném přístupu ke zdrojům aplikace například neoprávněným voláním url - v případě našeho příkladu by 
bylo možné při znalosti parametrů (není složité je z formuláře zjistit) založit neoprávněně kontakt.

Laravel jako ochranu při vytvoření session vytvoří [token](https://laravel.com/docs/5.6/csrf), proti kterému v middlewawe 
__VerifyCSRFToken__ kontroje vstupní data. Pro nás ve formuláři to znamená zavední pole _token helperem __csrf_field__,
který vygeneruje potřebné formulářové pole.

Ukázka výstupu helperu {{ csfr_field() }}.

~~~html
<input type="hidden" name="_token" value="xj6RNap7w92hpplPmVhl9l8RGhIc9zYH9fSte7In">
~~~

### Lokalizace textu formuláře
Nakonec provedeme lokalizaci fomuláře. Použijeme blade direktivu __@lang__ a texty přeložíme v přísušném souboru 
__resources/cs.json__. Další podrobnosti o lokalizaci v dokumentaci [laravelu](https://laravel.com/docs/5.6/localization).

```html
<form method="post">
   {{ csfr_field() }}
   <label for="first_name">@lang('First name')</label>
   <input name="first_name" id="first_name" />
   
   <label for="last_name">@lang('Last name')</label>
   <input name="last_name" id="last_name" />
   
   <label for="email">@lang('Email')</label>
   <input name="email" id="email" type="text"/>
   
   <input type="submit" name="save_data" value="@lang('Save data')"/>
</form>
```
To je pro úspěšné spuštění prvního funkčního formuláře všechno.
