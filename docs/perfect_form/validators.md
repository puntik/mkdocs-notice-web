### Zavedení validace v Controlleru

V první kapitole jsem vytvořili jednoduchý formulář se zpracovaním. Nyní opustíme naivní přístup ideálního světa a 
zavedeme validace uživatelského vstupu. Použijeme metodu kontrolleru __validate__. 

Metoda validate vrací jako výstup pole s validními daty. Pokud validace selže, Laravel do sessiony doplní kolekci chyb 
s popisem, která je ve view dostupná v proměnné __$errors__.

~~~php
<?php

class ContactController extends Controller {

	public function index() {
		$contacts = Contact::all();
		
		return view('contact.index', compact('contacts'));
	}
	
	public function create() {

		return view('contact.create');
	}
	
	public function store(Request $request) {
	
		/*
		 * validni data se vrátí jako výstup validační funkce
		 */
		$validated = $this->validate($request, [
			'first_name' => 'required',
			'last_name'  => 'required',
			'email'      => 'required|email'
		]); 
	
		Contact::create($validated);
		
		return view('contact.index');
	}
}
~~~

### Zobrazení chybových zpráv po validaci

Po chybě ve validaci máme tedy ve view kolekci $errors, která obsahuje informace o chybách. Je to kolekce, takže pro testování
můžeme použít oblíbenou metodu __has__ a k nalezení popisu chyby __first__.

Pro opětovné vypsání hodnoty použijeme helper __old()__.
~~~html
<form method="post">
	{{ csfr_field() }}
	<label for="first_name">First name</label>
	<input name="first_name" id="first_name" value="{{ old('first_name', '') }}" class="@if($errors->has('first_name')) is-invalid @endif"/>
	@if($errors->has('first_name'))
		<small class="text-danger">{{ $errors->first('first_name') }}</small>
	@endif
	
	<label for="last_name">Last name</label>
	<input name="last_name" id="last_name" value="{{ old('last_name', '') }}" class="@if($errors->has('last_name')) is-invalid @endif"/>
	@if($errors->has('last_name'))
		<small class="text-danger">{{ $errors->first('last_name') }}</small>
	@endif
</form>
~~~

Můžeme i projít celý error bag a nechat vypsat všechny chyby najednou.
~~~html
<form method="post">
	@if($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	{{ csfr_field() }}
	<label for="first_name">First name</label>
	<input name="first_name" id="first_name" value="{{ old('first_name', '') }}" class="@if($errors->has('first_name')) is-invalid @endif"/>
	@if($errors->has('first_name'))
		<small class="text-danger">{{ $errors->first('first_name') }}</small>
	@endif
	
	<label for="last_name">Last name</label>
	<input name="last_name" id="last_name" value="{{ old('last_name', '') }}" class="@if($errors->has('last_name')) is-invalid @endif"/>
	@if($errors->has('last_name'))
		<small class="text-danger">{{ $errors->first('last_name') }}</small>
	@endif
</form>
~~~
